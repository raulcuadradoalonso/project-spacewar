﻿Shader "Playground/LifeReactiveShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color("Grid color", Color) = (1,1,1,1)
		
		_MaxAlpha("Max Alpha", Range(0.0, 1.0)) = 0.8
		_BeatSpeed("Beat Speed", Range(0.0, 10.0)) = 1.0

		_CamDistThreshold("Wire Fadeout Distance Max", float) = 1.0
		_Decay("Fadeout decay", float) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Fade" "Queue" = "Transparent" "IgnoreProjector" = "True" }
        LOD 100
		
		ZTest On
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		
        Pass
        {
            CGPROGRAM
			
            #pragma vertex vert
            #pragma fragment frag
     
			
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;          
                float4 vertex : SV_POSITION;
				
            };

			struct v2f_wPos
			{
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
				float4 wPos : TEXCOORD1;
			};
			
			float dist2(float3 a, float3 b)
			{
				float3 v = a - b;
				return dot(v, v);
			}

			//Life variables
            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _Color;
			float _MaxAlpha;
			float _BeatSpeed;

			//DistanceVariables
			float _CamDistThreshold;
			float _Decay;

			float4 GetLifeColor(v2f_wPos i)
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				if (col.a != 0) { col.rgb = _Color.rgb; }
				float heartBeat = sin(_Time.y * _BeatSpeed);
				col.a = (heartBeat / 2) + 0.5;
				col.a *= _MaxAlpha;

				return col;
			}

            v2f_wPos vert (appdata v)
            {
                v2f_wPos o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.wPos = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            fixed4 frag (v2f_wPos i) : SV_Target
            {
				//Distance to change wiring color.
				float distFromCamera = dist2(_WorldSpaceCameraPos, i.wPos.xyz);
				float maxDist = _CamDistThreshold * _CamDistThreshold;
				float newColor = clamp(_Decay / (distFromCamera - maxDist), 0, 1);

                //Shield Life Region
                fixed4 life_col = GetLifeColor(i);

				//Distance to change wiring color.
				if (distFromCamera > maxDist)
				{	life_col.a *= newColor;		}
				if (_BeatSpeed == 0) { life_col.a = 0; }
				
				return life_col;
            }
            ENDCG
        }
    }
			Fallback "Diffuse"
}
