﻿Shader "Playground/SimpleOutlineVehicle"
{
	//Shader from: https://github.com/Shrimpey/Outlined-Diffuse-Shader-Fixed/blob/master/UniformOutline.shader

	Properties
	{
		_Color("Main Color", Color) = (0.5,0.5,0.5,1)
		_MainTex("Texture", 2D) = "white" {}
		_OutlineColor("Outline color", Color) = (0,0,0,1)
		_OutlineWidth("Outlines width", Range(0.0, 2.0)) = 1.1
		_OutlineColorInsideBomb("Outline color inside bomb", Color) = (1, 0.5, 0.5, 1)
		_BombRadius("Bomb radius", float) = -1.0
	}

		CGINCLUDE
		#include "UnityCG.cginc"

		struct appdata
		{
			float3 normal : NORMAL;
			float4 vertex : POSITION;
		};

		struct v2f
		{
			float4 pos : SV_POSITION;
		};

		struct v2f_wPos
		{
			float4 pos : SV_POSITION;
			float4 wPos : TEXCOORD0;
		};

		uniform float _OutlineWidth;
		uniform float4 _OutlineColor;
		uniform float4 _OutlineColorInsideBomb;
		uniform sampler2D _MainTex;
		uniform float4 _Color;
		uniform float _BombRadius;

		ENDCG

			SubShader
		{

			Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "DisableBatching" = "True"}

			Pass //Outline
			{
				Stencil
				{
					Ref 1
					Comp always
					Pass replace
				}

				ZWrite On
				Cull Front

				CGPROGRAM

				#pragma vertex vert
				#pragma fragment frag

				v2f vert(appdata v)
				{
					appdata original = v;

					float3 wpos = mul((float3x3)unity_ObjectToWorld, v.vertex.xyz);
					wpos.xyz += normalize(UnityObjectToWorldNormal(v.normal)) * _OutlineWidth;
					v.vertex.xyz = mul((float3x3)unity_WorldToObject, wpos);

					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);
					return o;
				}

				half4 frag(v2f i) : COLOR
				{
					return _OutlineColor;
				}
				ENDCG
			}

			Tags{ "Queue" = "Transparent"  "IgnoreProjector" = "True" "DisableBatching" = "True"}

			Pass //Object
			{
				Stencil
				{
					Ref 1
					Comp equal
					Pass replace
				}

				CGPROGRAM

				#pragma vertex vert
				#pragma fragment frag

				float4 _BombPos;

				float distance2(float4 a, float4 b)
				{
					float xDiff = b.x - a.x;
					float yDiff = b.y - a.y;
					float zDiff = b.z - a.z;

					return xDiff * xDiff + yDiff * yDiff + zDiff * zDiff;
				}

				v2f_wPos vert(appdata v)
				{
					v2f_wPos o;
					o.wPos = mul(unity_ObjectToWorld, v.vertex);
					o.pos = UnityObjectToClipPos(v.vertex);
					return o;
				}

				half4 frag(v2f_wPos i) : COLOR
				{
					float distance = distance2(i.wPos, _BombPos);

					if (_BombRadius > 0 && distance > _BombRadius)
					{
						return _OutlineColorInsideBomb;
					}

					return _Color;
				}
				ENDCG
			}
		}
		Fallback "Diffuse"
}