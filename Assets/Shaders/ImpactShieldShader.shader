﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Playground/ImpactShieldShader"
{
    Properties
    {
		_ImpactColor("Impact Color", Color) = (0.1, 0.4, 0.9, 1)
	}
    SubShader
    {
		Tags { "RenderType" = "Cutout" "IgnoreProjector" = "True" "DisableBatching" = "True"}
        LOD 100

		ZTest Off
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off

        Pass
        {
            CGPROGRAM

			#pragma target 3.0		
            #pragma vertex vert
            #pragma fragment frag
          

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
				float4 normal : NORMAL;
            };

            struct v2f
            {            
                float4 vertex : SV_POSITION;
				float4 wPos : TEXCOORD0;
				float v_normal : NORMAL;
            };

			//Impact Shield Variables
			float4 _ImpactColor;
			
			float4x4 _ImpactsMatrix;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.v_normal = normalize(v.normal);
				o.wPos = mul(unity_ObjectToWorld, v.vertex);
                return o;
            }

            fixed4 frag (v2f input) : SV_Target
            {
				//Shield Impact Region
				float impactValue = 0.0;
				
				//Algoritmo sacado de Sketchpunk Labs, episodio 64.1 de "Fun with WebGL".
				//Realizamos el dot de el punto de impacto y la normal de éste.
				//De esta forma sabemos la dirección y lugar del "anillo" a dibujar.
				
				float3 ImpactPos;
				float contactPoint;
				float angleMovement;
				float THICKness = 0.1f;

				float3 objVertex = mul(unity_WorldToObject, input.wPos);
				//float3 objVertex =  input.wPos;

				for (int i = 0; i < 4; i++)
				{
					if (_ImpactsMatrix[i].w != 1.5f)
					{
						ImpactPos = _ImpactsMatrix[i].xyz;
						contactPoint = dot(ImpactPos, normalize(objVertex));
						angleMovement = _ImpactsMatrix[i].w;

						if (contactPoint <= angleMovement && contactPoint >= angleMovement - THICKness)
						{
							impactValue += 1.0f;
						}
					}					
				}

				//Tratamos el color de impacto como si fuera emisivo.
                return (_ImpactColor * impactValue);
            }
            ENDCG
        }
	
	}
		FallBack "Diffuse"
}
