﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Playground/WiredOutline"
{	
	//Shader from: https://github.com/Shrimpey/Outlined-Diffuse-Shader-Fixed/blob/master/UniformOutline.shader
	Properties
	{
		_Color("Main Color", Color) = (0.5,0.5,0.5,1)
		_MainTex("Texture", 2D) = "white" {}
		_OutlineColor("Outline color", Color) = (0,0,0,1)
		_OutlineWidth("Outline width", Range(0.0, 2.0)) = 1.1
		_WireWidth("Wire Width", Range(0.0, 0.5)) = 0.1
		_WireColor("Wire color", Color) = (0,0,0,1)
		_CamDistThreshold("Wire Fadeout Distance Max", float) = 1.0
		_Decay("Fadeout decay", float) = 1.0

	}

		CGINCLUDE
		#include "UnityCG.cginc"

		struct appdata
		{	
			float3 normal : NORMAL;
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float4 pos : POSITION;
		};
		struct v2f_wPos
		{
			float4 pos : POSITION;
			float2 uv : TEXCOORD0;
			float4 wPos : TEXCOORD1;
		};

		float dist2(float3 a, float3 b)
		{
			float3 v = a - b;
			return dot(v, v);
		}


		uniform float _OutlineWidth;
		uniform float4 _OutlineColor;
		uniform sampler2D _MainTex;
		float4 _MainTex_ST;
		uniform float4 _Color;
		half _WireWidth;
		float4 _WireColor;
		float _CamDistThreshold;
		float _Decay;
		ENDCG

		SubShader
		{
			Tags{ "RenderType" = "Opaque" "IgnoreProjector" = "True" "DisableBatching" = "True"}

			Pass //Outline
			{
				ZWrite Off
				Cull Off

				CGPROGRAM

				#pragma vertex vert
				#pragma fragment frag

				v2f vert(appdata v)
				{
					appdata original = v;

					float3 wpos = mul((float3x3)unity_ObjectToWorld, v.vertex.xyz);
					wpos.xyz += normalize(UnityObjectToWorldNormal(v.normal)) * _OutlineWidth;
					v.vertex.xyz = mul((float3x3)unity_WorldToObject, wpos);

					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex.xyz);
					return o;
				}

				half4 frag(v2f i) : COLOR
				{
					return _OutlineColor;
				}
				ENDCG
			}

			Tags{ "RenderType" = "Opaque"  "IgnoreProjector" = "True"}

			Pass
			{
				CGPROGRAM

				#pragma vertex vert
				#pragma fragment frag


				v2f_wPos vert(appdata v)
				{
					v2f_wPos o;

					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					o.pos = UnityObjectToClipPos(v.vertex);
					o.wPos = mul(unity_ObjectToWorld, v.vertex);
					return o;
				}

				half4 frag(v2f_wPos i) : COLOR
				{
					//Distance to change wiring color.
					float distFromCamera = dist2(_WorldSpaceCameraPos, i.wPos.xyz);
					float maxDist = _CamDistThreshold * _CamDistThreshold;

					if (distFromCamera > maxDist)
					{
						float newColor = clamp(abs(_Decay / (distFromCamera - maxDist)), 0, 1);
						_WireColor.rgb = _WireColor.rgb * newColor;
					}

					//texture and wiring
					fixed4 col = tex2D(_MainTex, i.uv);
					if ((frac(i.uv.x) < _WireWidth ||
						frac(i.uv.x) > 1 - _WireWidth) ||
						(frac(i.uv.y) < _WireWidth ||
						frac(i.uv.y) > 1 - _WireWidth))
					{
						return _WireColor;
					}
					else
					{
						return _Color;
					}

				}
				ENDCG
			}

		}
			Fallback "Diffuse"
}