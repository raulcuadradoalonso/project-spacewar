﻿Shader "Unlit/BorderHighlightShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_OutlineWidth("Outline Width", Range(0.0, 0.5)) = 0.1
		_OutlineColor("Outline color", Color) = (0,0,0,1)
		_FillColor("Fill color", Color) = (0,0,0,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			half _OutlineWidth;
			float4 _OutlineColor;
			float4 _FillColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);

				if ((frac(i.uv.x) < _OutlineWidth ||
					frac(i.uv.x) > 1 - _OutlineWidth)  ||
					(frac(i.uv.y) < _OutlineWidth ||
					frac(i.uv.y) > 1 - _OutlineWidth))
				{
					return _OutlineColor;
				}
				else
				{
					return _FillColor;
				}

              //  return col;
            }
            ENDCG
        }
    }
}
