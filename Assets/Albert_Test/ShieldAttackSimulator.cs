﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShieldAttackSimulator : MonoBehaviour
{
    public float radius;
    float timer;
    public float SecondsPerShot = 1.0f;
    public ShieldImpact controller;
    public CompleteShield controller_full;
    bool firstRay;

    // Start is called before the first frame update
    void Start()
    {
        firstRay = false;
        timer = 0.0f;
        controller = GetComponent<ShieldImpact>();
        if (controller == null)
        { controller_full = GetComponent<CompleteShield>(); }
        
    }

    // Update is called once per frame
    void Update()
    {
            timer += Time.deltaTime;
            if (timer >= SecondsPerShot)
            {
                timer = 0.0f;
                Vector3 rand_Pos = Random.onUnitSphere * radius;
                if (controller == null)
                {
                controller_full.AddDataPoint(rand_Pos + transform.position);
                controller_full.ReceiveDamage(10f);
                }
                else
                {
                controller.AddDataPoint(rand_Pos + transform.position);
                }
                
            }   
    }
}
