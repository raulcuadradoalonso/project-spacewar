﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldLife : MonoBehaviour
{
    [Header("Shader Life Variables")]
    Material shaderReference;
    public Color DangerZoneColor = Color.red;
    public float MaxHealth = 100;
    public float currentHealth;
    [Range(0f, 100f)]
    public float DangerThreshold = 30f;

    // Start is called before the first frame update
    void Start()
    {
        shaderReference = this.GetComponent<MeshRenderer>().material;
        currentHealth = MaxHealth;
        UpdateShaderColor();      

    }

    // Update is called once per frame
    void Update()
    {
        #region SHADER_LIFE_FUNCTIONS
        if (shaderReference.color == Color.white && currentHealth <= MaxHealth * DangerThreshold / 100f)
        { UpdateShaderColor(); }
        if (shaderReference.color == DangerZoneColor && currentHealth > MaxHealth * DangerThreshold / 100f)
        { UpdateShaderColor(); }
        UpdateShaderBeatSpeed();
        #endregion

        
    }
    #region SHADER_LIFE_FUNCTIONS
    void UpdateShaderColor()
    {
        if (currentHealth <= MaxHealth * DangerThreshold / 100f)
        {   shaderReference.SetColor("_Color", DangerZoneColor);    }
        else
        {   shaderReference.SetColor("_Color", Color.white);        }
    }

    void UpdateShaderBeatSpeed()
    {
        //Valores entre los que va: 0 a 10
        //Vida: de MaxHealth a 0.
        //MaxHealth menos currentHealth: cambio ascendente.
        //Dividimos entre MaxHealth: hacemos que el rango vaya de 0 a 1.
        float beatSpeed = (MaxHealth - currentHealth) / MaxHealth;
        //Multiplicamos por 10 para que vaya de 0 a 10.
        beatSpeed *= 10.0f;
        if (beatSpeed < DangerThreshold / 200) { beatSpeed = 1.0f; }
        else if (beatSpeed / 10.0f < DangerThreshold / 100) { beatSpeed = 4.0f; }
        if (currentHealth == 0) { beatSpeed = 0.0f; }
        shaderReference.SetFloat("_BeatSpeed", beatSpeed );
    }
    #endregion    
}
