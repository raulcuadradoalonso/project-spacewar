﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class CompleteShield : MonoBehaviour
{
    #region IMPACT_SHIELD_VARIABLES
    [Header("Shader Impact Variables")]
    /// <summary>
    /// Tiempo que tarda un impacto en desaparecer.
    /// </summary>
    public float Decay = 4.0f;    
    //System works with a matrix.
    public Matrix4x4 Impacts_Test;
    #endregion

    #region LIFE_SHIELD_VARIABLES
    [Header("Shader Life Variables")]
    public Color baseShieldColor = Color.white;
    public Color DangerZoneColor = Color.red;
    public float MaxHealth = 100;
    public float currentHealth;
    [Range(0f, 1f)]
    public float DangerThreshold = 0.3f;
    #endregion

    public GameObject deathParticleSystem;
    Material shaderReference;

    #region MONOBEHAVIOUR_FUNCTIONS

    void Start()
    {
        shaderReference = this.GetComponent<MeshRenderer>().material;
        //Inicializamos matriz
        Impacts_Test = new Matrix4x4(new Vector4(0, 0, 0, 0), new Vector4(0, 0, 0, 0),
                                     new Vector4(0, 0, 0, 0), new Vector4(-1.5f, -1.5f, -1.5f, -1.5f));
        //INICIALIZAMOS VIDA Y COLOR EN SHADER
        currentHealth = MaxHealth;
        UpdateShaderColor();

    }

    // Update is called once per frame
    void Update()
    {
        //Añadir la rotacion inversa al nuevo vector obtenido, 
        // para que el efecto se haga bien...
        if (transform.parent != null)
        { transform.localRotation = Quaternion.Inverse(transform.parent.rotation); }

        #region IMPACT_SHIELD_FUNCTIONS
        UpdateShaderShieldVariables();
        #endregion

        #region LIFE_SHIELD_FUNCTIONS
        
         UpdateShaderColor(); 
       
        UpdateShaderBeatSpeed();
        #endregion

    }

    #endregion

    #region IMPACT_SHIELD_FUNCTIONS
    void UpdateShaderShieldVariables()
    {
        UpdateImpactMatrix();
        shaderReference.SetMatrix("_ImpactsMatrix", Impacts_Test);
    }

    /// <summary>
    /// Actualizacion de la matriz de impactos.
    /// De haber pasado el tiempo de impacto, se borra la referencia.
    /// Si no, se actualiza el factor de tiempo.
    /// </summary>
    void UpdateImpactMatrix()
    {
        for (int i = 0; i < 4; i++)
        {
            if (Impacts_Test[i, 3] != -1.5f)
            {
                //Setteamos el tiempo del contacto, para que haga bien el ripple.
                float newWeight = Mathf.Clamp(Impacts_Test[i, 3] - (Time.deltaTime / Decay), -1.0f, 1.0f);
                if (newWeight == -1.0f)
                {
                    Impacts_Test.SetRow(i, new Vector4(0.0f, 0.0f, 0.0f, -1.5f));
                    //Impacts_Test[i, 3] = -1.5f;
                }
                else
                { Impacts_Test[i, 3] = newWeight; }

            }
        }
        shaderReference.SetMatrix("_ImpactsMatrix", Impacts_Test);
    }


    /// <param name="pos">
    /// Coordenadas globales del punto de impacto.
    /// </param>
    public void AddDataPoint(Vector3 pos)
    {
        //La W irá de 1 a -1, para que recorra toda la esfera.
        //La dirección es desde el centro de la esfera al punto de contacto.
        //esta dirección debe normalizarse para que el shader funcione bien.
        //Algoritmo sacado de Sketchpunk Labs, episodio 64.1 de "Fun with WebGL".

        pos = (pos - transform.position).normalized;

        float minW = 2.0f;
        int minIndex = 0;
        for (int i = 0; i < 4; i++)
        {
            if (Impacts_Test[i, 3] == -1.5f)
            {
                minIndex = i;
                break;
            }
            else if (Impacts_Test[i, 3] <= minW)
            {
                minW = Impacts_Test[i, 3];
                minIndex = i;
            }
        }


        Impacts_Test.SetRow(minIndex, new Vector4(pos.x, pos.y, pos.z, 1.0f));
        //Debug.DrawRay(transform.position, pos * 5, Color.red, 3.0f);
        //*/
    }

    #endregion

    #region LIFE_SHIELD_FUNCTIONS
    
    void UpdateShaderColor()
    {
        if (currentHealth <= MaxHealth * DangerThreshold)
        { shaderReference.SetColor("_Color", DangerZoneColor); }
        else
        { shaderReference.SetColor("_Color", baseShieldColor); }
    }

    void UpdateShaderBeatSpeed()
    {
        //Valores entre los que va: 0 a 10
        //Vida: de MaxHealth a 0.
        //MaxHealth menos currentHealth: cambio ascendente.
        //Dividimos entre MaxHealth: hacemos que el rango vaya de 0 a 1.
        float beatSpeed = (MaxHealth - currentHealth) / MaxHealth;
        //Multiplicamos por 10 para que vaya de 0 a 10.
        beatSpeed *= 10.0f;
        if (beatSpeed < DangerThreshold / 200) { beatSpeed = 1.0f; }
        else if (beatSpeed / 10.0f < DangerThreshold / 100) { beatSpeed = 4.0f; }
        if (currentHealth == 0) { beatSpeed = 0.0f; }
        shaderReference.SetFloat("_BeatSpeed", beatSpeed);
    }
    
    public void ReceiveDamage(float damage)
    {
        currentHealth  = Mathf.Clamp(currentHealth -damage, 0.0f, MaxHealth);
        //Si vida es 0, instanciamos particulas en el punto que toca y borramos este 
        //objeto de la faz de la tierra.
        if (currentHealth == 0.0f)
        {
            GameObject partSys = Instantiate(deathParticleSystem, transform.position, transform.rotation);
            Destroy(partSys, 2.0f);
            Destroy(transform.parent.gameObject, 0.1f);
        }
    }
    
    #endregion
    


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "EnemyBullet")
        {
            AddDataPoint(other.transform.position);
            //Destroy(other.gameObject);
        }
    }









}
