﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShieldImpact : MonoBehaviour
{
    [Header("Shader Impact Variables")]
    //Code from: https://www.youtube.com/watch?v=NeZcAYJdkv4&list=PLEwYhelKHmig6ttzH0nRL3OOQsGLtVrtX&index=36&t=0s
   
    /// <summary>
    /// Tiempo que tarda un impacto en desaparecer.
    /// </summary>
    public float Decay = 4.0f;
       
    Material shaderReference;

    //System works with a matrix.
    public Matrix4x4 Impacts_Test;

    // Start is called before the first frame update
    void Start()
    {
        shaderReference = this.GetComponent<MeshRenderer>().material;
        //Inicializamos matriz
        Impacts_Test = new Matrix4x4(new Vector4(0, 0, 0, 0), new Vector4(0, 0, 0, 0),
                                     new Vector4(0, 0, 0, 0), new Vector4(-1.5f, -1.5f, -1.5f, -1.5f));

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            AddDataPoint(Vector3.forward);
        }

        //Añadir la rotacion inversa al nuevo vector obtenido, 
        // para que el efecto se haga bien...
        if (transform.parent != null)
        { transform.localRotation = Quaternion.Inverse(transform.parent.rotation); }

        #region SHADER_SHIELD_FUNCTIONS
        UpdateShaderShieldVariables();
        #endregion
    }

    #region SHADER_SHIELD_FUNCTIONS
    void UpdateShaderShieldVariables()
    {
        UpdateImpactMatrix();
        shaderReference.SetMatrix("_ImpactsMatrix", Impacts_Test);
    }

    /// <summary>
    /// Actualizacion de la matriz de impactos.
    /// De haber pasado el tiempo de impacto, se borra la referencia.
    /// Si no, se actualiza el factor de tiempo.
    /// </summary>
    void UpdateImpactMatrix()
    {
        for (int i = 0; i < 4; i++)
        {
            if (Impacts_Test[i,3] != -1.5f)
            {
                //Setteamos el tiempo del contacto, para que haga bien el ripple.
                float newWeight = Mathf.Clamp(Impacts_Test[i, 3] - (Time.deltaTime / Decay), -1.0f, 1.0f);
                if (newWeight == -1.0f)
                {
                    Impacts_Test.SetRow(i, new Vector4(0.0f, 0.0f, 0.0f, -1.5f));
                    //Impacts_Test[i, 3] = -1.5f;
                }
                else
                { Impacts_Test[i, 3] = newWeight; }
                
            }            
        }
        shaderReference.SetMatrix("_ImpactsMatrix", Impacts_Test);
    }

    
    /// <param name="pos">
    /// Coordenadas globales del punto de impacto.
    /// </param>
    public void AddDataPoint(Vector3 pos)
    {
        //La W irá de 1 a -1, para que recorra toda la esfera.
        //La dirección es desde el centro de la esfera al punto de contacto.
        //esta dirección debe normalizarse para que el shader funcione bien.
        //Algoritmo sacado de Sketchpunk Labs, episodio 64.1 de "Fun with WebGL".

        pos = (pos - transform.position).normalized;
       
        float minW = 2.0f;
        int minIndex = 0;
        for (int i = 0; i < 4; i++)
        {
            if (Impacts_Test[i, 3] == -1.5f)
            {
                minIndex = i;
                break;
            }
            else if (Impacts_Test[i, 3] <= minW)
            {
                minW = Impacts_Test[i, 3];
                minIndex = i;
            }
        }
       

        Impacts_Test.SetRow(minIndex, new Vector4(pos.x, pos.y, pos.z, 1.0f));
        Debug.DrawRay(transform.position, pos * 5, Color.red, 3.0f);
        //*/
    }

    #endregion

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "EnemyBullet")
        {
            AddDataPoint(other.transform.position);
            //Destroy(other.gameObject);
        }
    }
}
