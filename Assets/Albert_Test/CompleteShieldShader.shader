﻿Shader "Playground/CompleteShieldShader"
{
    Properties
    {
		_MainTex("Texture", 2D) = "white" {}

		_MaxAlpha("Max Alpha", Range(0.0, 1.0)) = 0.8
		_MinAlpha("Min Alpha", Range(0.0, 1.0)) = 0.05

		_BeatSpeed("Beat Speed", Range(0.0, 10.0)) = 1.0

		_CamDistThreshold("Wire Fadeout Distance Max", float) = 1.0
		_Decay("Fadeout decay", float) = 1.0

		_ImpactColor("Impact Color", Color) = (0.1, 0.4, 0.9, 1)
    }
    SubShader
    {
		Tags { "RenderType" = "Fade" "Queue" = "Transparent" "IgnoreProjector" = "True" }
		LOD 100

		ZTest Off
		Blend SrcAlpha OneMinusSrcAlpha
		//Cull Back

	   Pass
		{
			CGINCLUDE

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;

			};

			struct v2f_wPos
			{
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
				float4 wPos : TEXCOORD1;
			};

			float dist2(float3 a, float3 b)
			{
				float3 v = a - b;
				return dot(v, v);
			}

			//Life variables
			sampler2D _MainTex;
			float4 _MainTex_ST;
			//Se maneja desde shader
			float4 _Color;
			float _MaxAlpha;
			float _MinAlpha;
			float _BeatSpeed;

			//DistanceVariables
			float _CamDistThreshold;
			float _Decay;

			//Impact Shield Variables
			float4 _ImpactColor;

			float4x4 _ImpactsMatrix;

			float4 GetLifeColor(v2f_wPos i)
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				if (col.a != 0) { col.rgb = _Color.rgb; }
				float heartBeat = sin(_Time.y * _BeatSpeed);
				col.a = (heartBeat / 2) + 0.5;
				col.a *= _MaxAlpha;
				return col;
			}

			float4 getImpactColor(v2f_wPos input)
			{
				//Shield Impact Region
				float impactValue = 0.0;

				//Algoritmo sacado de Sketchpunk Labs, episodio 64.1 de "Fun with WebGL".
				//Realizamos el dot de el punto de impacto y la normal de éste.
				//De esta forma sabemos la dirección y lugar del "anillo" a dibujar.

				float3 ImpactPos;
				float contactPoint;
				float angleMovement;
				float THICKness = 0.1f;

				float3 objVertex = mul(unity_WorldToObject, input.wPos);
				//float3 objVertex =  input.wPos;

				for (int i = 0; i < 4; i++)
				{
					if (_ImpactsMatrix[i].w != 1.5f)
					{
						ImpactPos = _ImpactsMatrix[i].xyz;
						contactPoint = dot(ImpactPos, normalize(objVertex));
						angleMovement = _ImpactsMatrix[i].w;

						if (contactPoint <= angleMovement && contactPoint >= angleMovement - THICKness)
						{
							//impactValue += 1.0f;
							impactValue += 1.25f;
						}
					}
				}
				return (_ImpactColor * impactValue);

			}

			v2f_wPos vert(appdata v)
			{
				v2f_wPos o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.wPos = mul(unity_ObjectToWorld, v.vertex);
				return o;
			}

			fixed4 frag(v2f_wPos i) : SV_Target
			{
				//Distance to change wiring color.
				float distFromCamera = dist2(_WorldSpaceCameraPos, i.wPos.xyz);
				float maxDist = _CamDistThreshold * _CamDistThreshold;
				float newColor = clamp(_Decay / (distFromCamera - maxDist), 0, 1);

				//Shield Life Region
				fixed4 life_col = GetLifeColor(i);
				life_col.a += _MinAlpha;
				//Distance to change wiring color.
				//if (distFromCamera > maxDist)
				//{
				//	//life_col.a *= newColor;
				//}
				if (_BeatSpeed == 0) { life_col.a = 0; }
				float4 impactColor = getImpactColor(i);
				
				if (impactColor.a == 0.0f)
				{
					return life_col;
				}
				if (life_col.a != 0.0f && 
					(life_col.r != 0.0f || life_col.g != 0.0f || life_col.b != 0.0f))
				{
					return impactColor;
				}
				//Suma de color
				//return impactColor + life_col;
				return life_col * impactColor;
				
			}
			ENDCG

			Cull Front
			
			CGPROGRAM
			
				
				#pragma vertex vert
				#pragma fragment frag

			ENDCG
		}

		Pass
		{
			Cull Back

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"


			ENDCG
		}



    }
}
