﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Mathf2
{
    public static float Map(float to, float from, float to2, float from2, float value)
    {
        if (value <= from2)
        {
            return from;
        }
        else if (value >= to2)
        {
            return to;
        }
        else
        {
            return (to - from) * ((value - from2) / (to2 - from2)) + from;
        }
    }

    public static float TwoGradEc(float a, float b, float c)
    {
        float x = (-b + Mathf.Sqrt((b * b) - (4 * a * c))) / (2 * a);

        if (x < 0)
            x = (-b - Mathf.Sqrt((b * b) - (4 * a * c))) / (2 * a);
        if (x < 0)
            throw new System.Exception($"Mathf2 is returning a negative value");
        return x;
    }
}