﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    [Header("Canvas References")]
    public GameObject level_selection_canvas_GO_reference;
    public GameObject play_game_canvas_GO_reference;
    public GameObject exit_game_canvas_GO_reference;
    public GameObject fade_to_scene_GO_reference;     

    [Header("Scene names")]
    public string sceneNameLV1 = "";
    public string sceneNameLV2 = "";
    public string sceneNameLV3 = "";
    public string sceneNameLV4 = "";


    bool animStarted;
    string sceneToLoad;
    float animationTimer;
    Image fade_to_scene_Image_reference;

    // Start is called before the first frame update
    void Start()
    {
        ChangeState(0);
        animationTimer = 0.0f;
        animStarted = false;
        fade_to_scene_Image_reference = fade_to_scene_GO_reference.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (animStarted)
        {
            animationTimer += Time.deltaTime * 0.75f;
            fade_to_scene_Image_reference.color = new Color(0.0f, 0.0f, 0.0f, animationTimer);
            if (animationTimer >= 1.0f)
            {
                SceneManager.LoadScene(sceneToLoad);
            }
        }
    }


    public void ChangeToScene( int levelReference)
    {
        switch (levelReference)
        {
            //1: primer nivel
            case 1:
                {
                    ChangeState(2);
                    sceneToLoad = sceneNameLV1;
                    animStarted = true;
                    break;
                }
            case 2:
                {
                    ChangeState(2);
                    sceneToLoad = sceneNameLV2;
                    animStarted = true;
                    break;
                }
            case 3:
                {
                    ChangeState(2);
                    sceneToLoad = sceneNameLV3;
                    animStarted = true;
                    break;
                }
            case 4:
                {
                    ChangeState(2);
                    sceneToLoad = sceneNameLV4;
                    animStarted = true;
                    break;
                }
        }
                    
       // print("new Scene load!!");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ChangeState(int newState)
    {
        switch(newState)
        {
            //0: estado inicial
            case 0:
                {
                    level_selection_canvas_GO_reference.SetActive(false);
                    exit_game_canvas_GO_reference.SetActive(true);
                    play_game_canvas_GO_reference.SetActive(true);
                    fade_to_scene_GO_reference.SetActive(false);
                    break;
                }
            //1: estado seleccion de nivel
            case 1:
                {
                    level_selection_canvas_GO_reference.SetActive(true);
                    exit_game_canvas_GO_reference.SetActive(false);
                    play_game_canvas_GO_reference.SetActive(false);
                    fade_to_scene_GO_reference.SetActive(false);
                    break;
                }
            //2: estado animacion
            case 2:
                {
                    level_selection_canvas_GO_reference.SetActive(false);
                    exit_game_canvas_GO_reference.SetActive(false);
                    play_game_canvas_GO_reference.SetActive(false);
                    fade_to_scene_GO_reference.SetActive(true);
                    break;
                }
            default:
                {
                    Debug.Log("you should not be here u piece of shit");
                    break;
                }

        }
    }
    

}
