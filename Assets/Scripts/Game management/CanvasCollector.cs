﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasCollector : MonoBehaviour
{
    [Header("Scene references")]
    public string main_menu_scene_reference;
    public string next_level_scene_reference;

    [Header("GROUP - First phase section")]
    public GameObject PanelFirstPhase;
    public Button ButonReady;
    public Button ButtonBasicTurret;
    public Button ButtonHeavyTurret;

    [Header("GROUP - Second phase section")]
    public GameObject PanelSecondPhase;
    public Button ButtonPowerUpBomb;
    public Button ButtonPowerUpBoost;

    [Header("GROUP - Game stats")]
    public GameObject PanelGameStats;
    public Text TextRemainingTime;
    public GameObject EnergyTextObject;
    public Text TextEnergy;
    public Text TextEnergyNumber;
    public Text TextNotEnoughEnergy;

    [Header("GROUP - Game info")]
    public GameObject PanelGameInfo;
    public Button ButtonContinue;
    public Button ButtonRetry;
    public Button ButtonExit;
    public Text TextBattleBegins;
    public Text TextVictory;
    public Text TextDefeat;

    [Header("Others")]
    public Image ImageAuxFader;
    public Image ImageReticle;

    public IEnumerator blinkingText(Text t)
    {
        t.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.4f);

        t.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.4f);

        t.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.4f);

        t.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.4f);

        t.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.4f);

        t.gameObject.SetActive(false);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(main_menu_scene_reference);
    }
    public void GoToNextLevel()
    {
        SceneManager.LoadScene(next_level_scene_reference);
    }
}
