﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.Tween;

public class GeneralManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private CameraManager camManager;
    [SerializeField] private LevelInterface currentLevel;
    [SerializeField] private CanvasCollector canvasCollector;

    [Header("Game stats")]
    [SerializeField] private float totalEnergy = 100f;
    [SerializeField] private float energyPerMinute = 50f;
    [SerializeField] private float gameTime = 120;
    [SerializeField] private float sliderTranstiionTime = 1.0f;
    [SerializeField] private float fadingTime;

    [Header("Other settings")]
    [SerializeField] private bool checkEndGame;
    [SerializeField] private bool enableTransitions;

    public GameObject debugTurret;

    private enum GAME_STATUS
    {
        BEGINNING,
        POSITIONING,
        BATTLE,
        END
    }

    private bool beginning = false;
    private bool positioning = false;
    private bool battle = false;
    private bool endMessage = false;
    private bool victory = false;

    //This number is incremented when the player sets turrets over the map
    //when it reaches 0, the player loses the game
    private List<GameObject> turrets;
    private List<GameObject> vehicles;

    //The number displayed in the screen is different from the 'real' one
    private float currentTextEnergy;
    private float currentEnergy;
    private bool updatingEnergyText = false;

    private float currentEnergyIncrement;
    private float second;

    private GAME_STATUS status;
    private float remainingTime;
   

    private void Start()
    {
        status = GAME_STATUS.BEGINNING;

        //Initial settings
        turrets = new List<GameObject>();
        vehicles = new List<GameObject>();

        canvasCollector.TextEnergyNumber.text = "" + totalEnergy;
        currentEnergy = totalEnergy;
        currentTextEnergy = totalEnergy;

        camManager.enabled = false;
        canvasCollector.PanelFirstPhase.SetActive(false);
        canvasCollector.PanelSecondPhase.SetActive(false);

        canvasCollector.ImageAuxFader.gameObject.SetActive(true);
        canvasCollector.ButonReady.gameObject.SetActive(false);
        canvasCollector.TextRemainingTime.gameObject.SetActive(false);

        remainingTime = gameTime;

        currentEnergyIncrement = energyPerMinute / 60f;
    }

    private void Update()
    {
        checkGameStatus();        

#if UNITY_EDITOR
        //if (Input.GetKeyDown(KeyCode.B))
        //{
        //    camManager.enabled = false;
        //}
        //if (Input.GetKeyDown(KeyCode.R))
        //{
        //    camManager.enabled = true;
        //}
#endif
    }

    #region Status Methods

    private void showRemainingTime()
    {
        remainingTime -= Time.deltaTime;

        int min = Mathf.FloorToInt(remainingTime / 60);
        int sec = Mathf.FloorToInt(remainingTime % 60);
        canvasCollector.TextRemainingTime.text = min.ToString("00") + ":" + sec.ToString("00");

        second += Time.deltaTime;
        if (second >= 1)
        {
            currentEnergy += currentEnergyIncrement;
            second = 0;
            if (!updatingEnergyText) { canvasCollector.TextEnergyNumber.text = "" + (int)currentEnergy; };
        }
    }

    private void checkGameStatus()
    {
        switch (status)
        {
            case GAME_STATUS.BEGINNING:

                if (!beginning)
                {
                    beginning = true;

                    if (enableTransitions)
                    {
                        StartCoroutine(transitionBeginning());
                    }
                    else
                    {
                        fastTransitionBeginning();
                    }
                }
                break;

            case GAME_STATUS.POSITIONING:

                if (!positioning)
                {
                    positioning = true;

                    //Give the control to the player
                    camManager.enabled = true;

                    //Show First Phase UI
                    canvasCollector.PanelFirstPhase.SetActive(true);
                }
                break;

            case GAME_STATUS.BATTLE:

                if (!battle)
                {
                    battle = true;

                    //Enable the second phase in the general view camera
                    camManager.EndPositioningTurrets();

                    //Swap UI for second phase
                    canvasCollector.PanelFirstPhase.SetActive(false);
                    canvasCollector.PanelSecondPhase.SetActive(true);
                    canvasCollector.TextRemainingTime.gameObject.SetActive(true);

                    //Enable level
                    currentLevel.EnableLevel();
                }

                showRemainingTime();
                if (checkEndGame) { checkForEndGame(); }

                break;

            case GAME_STATUS.END:

                if (!endMessage)
                {
                    endMessage = true;

                    //Disable player control
                    camManager.enabled = false;

                    //Hide UI
                    canvasCollector.PanelSecondPhase.SetActive(false);
                    canvasCollector.ImageReticle.gameObject.SetActive(false);

                    showEndUI(victory);
                }
                break;

            default:

                Debug.LogError("ERROR: Game status reached default!");
                break;
        }
    }

    private void checkForEndGame()
    {
        if (remainingTime > 0)
        {
            if (GetTurrets().Count <= 0)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                victory = false;
                status = GAME_STATUS.END;
                Debug.Log("Defeat!");
            }
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            victory = true;
            status = GAME_STATUS.END;
            Debug.Log("Victory!");
        }
    }

    #endregion Status Methods

    #region Transition Methods

    private IEnumerator transitionBeginning()
    {
        yield return new WaitForSeconds(1.0f);

        //Show initial message
        canvasCollector.TextBattleBegins.gameObject.SetActive(true);
        canvasCollector.TextBattleBegins.canvasRenderer.SetAlpha(1);

        yield return new WaitForSeconds(1.5f);

        //Hide message
        canvasCollector.TextBattleBegins.gameObject.Tween("Fade out", 1.0f, 0.0f, fadingTime, TweenScaleFunctions.CubicEaseInOut,
            (t) =>
            {
                //Progress
                canvasCollector.TextBattleBegins.canvasRenderer.SetAlpha(t.CurrentValue);
            });

        //Fade in from black screen
        canvasCollector.ImageAuxFader.canvasRenderer.SetAlpha(1);
        canvasCollector.ImageAuxFader.gameObject.Tween("Fade in", 1.0f, 0.0f, fadingTime, TweenScaleFunctions.CubicEaseInOut,
            (t) =>
            {
                //Progress
                canvasCollector.ImageAuxFader.canvasRenderer.SetAlpha(t.CurrentValue);
            });

        yield return new WaitForSeconds(fadingTime * 1.1f);

        //First phase begins
        status = GAME_STATUS.POSITIONING;
    }

    private IEnumerator transitionEnding(Text endingText, Button optionButton)
    {
        //Fade in from black screen
        canvasCollector.ImageAuxFader.canvasRenderer.SetAlpha(0);
        canvasCollector.ImageAuxFader.gameObject.Tween("Fade out", 0.0f, 1.0f, fadingTime, TweenScaleFunctions.CubicEaseInOut,
            (t) =>
            {
                //Progress
                //Debug.Log(t.CurrentValue);
                canvasCollector.ImageAuxFader.canvasRenderer.SetAlpha(t.CurrentValue);
            });

        //Show ending text
        endingText.gameObject.SetActive(true);
        endingText.canvasRenderer.SetAlpha(0);
        endingText.gameObject.Tween("Fade in", 0.0f, 1.0f, fadingTime, TweenScaleFunctions.CubicEaseInOut,
            (t) =>
            {
                //Progress
                endingText.canvasRenderer.SetAlpha(t.CurrentValue);
            });

        yield return new WaitForSeconds(fadingTime * 1.3f);

        //Show buttons
        optionButton.gameObject.SetActive(true);
        canvasCollector.ButtonExit.gameObject.SetActive(true);
    }

    //For debugging purposes, if enableTransitions is false
    //the game will just update the values of the alpha instantaneously
    private void fastTransitionBeginning()
    {
        canvasCollector.ImageAuxFader.canvasRenderer.SetAlpha(0);
        status = GAME_STATUS.POSITIONING;
    }

    private void fastTransitionEnding(Text endingText, Button optionButton)
    {
        canvasCollector.ImageAuxFader.canvasRenderer.SetAlpha(1);
        endingText.gameObject.SetActive(true);
        endingText.canvasRenderer.SetAlpha(1);
        optionButton.gameObject.SetActive(true);
        canvasCollector.ButtonExit.gameObject.SetActive(true);
    }

    private void showEndUI(bool playerWon)
    {
        Text endingText = (victory) ? canvasCollector.TextVictory : canvasCollector.TextDefeat;
        Button optionButton = (victory) ? canvasCollector.ButtonContinue : canvasCollector.ButtonRetry;

        //Show final UI
        if (enableTransitions)
        {
            StartCoroutine(transitionEnding(endingText, optionButton));
        }
        else
        {
            fastTransitionEnding(endingText, optionButton);
        }
    }

    public void EnableReadyButton()
    {
        canvasCollector.ButonReady.gameObject.SetActive(true);
    }

    public void SecondPhaseUIStatus(bool status)
    {
        canvasCollector.PanelSecondPhase.SetActive(status);
    }

    public void OnClickEndFirstPhase()
    {
        //Change game status
        status = GAME_STATUS.BATTLE;
    }

    #endregion Transition Methods

    #region Gameplay Methods

    public void payEnergyCost(float cost)
    {
        //Update the real value
        currentEnergy -= cost;

        float targetValue = currentEnergy;
        updatingEnergyText = true;

        canvasCollector.TextEnergyNumber.gameObject.Tween("Modifying energy bar", currentTextEnergy, targetValue, sliderTranstiionTime, TweenScaleFunctions.Linear, (t) =>
        {
            //Progress
            canvasCollector.TextEnergyNumber.text = "" + (int)t.CurrentValue;
            currentTextEnergy = t.CurrentValue;
        }, (t) =>
        {
            updatingEnergyText = false;
        });
    }

    //When using powerups, we must disable the functions of the camera that allows the player to select turrets
    public void disableCameraTurretSelector()
    {
        camManager.disableCameraTurretSelector();
    }

    public void enableCameraTurretSelector()
    {
        camManager.enableCameraTurretSelector();
    }

    public float GetCurrentEnergy()
    {
        return currentEnergy;
    }

    public bool IsAffordable(float cost)
    {
        if (currentEnergy - cost >= 0f)
        {
            return true;
        }

        //Show info text 'not enough energy!'
        StartCoroutine(canvasCollector.blinkingText(canvasCollector.TextNotEnoughEnergy));

        return false;
    }

    public void AddTurret(GameObject t)
    {
        turrets.Add(t);
    }

    public void AddVehicles(GameObject v)
    {
        vehicles.Add(v);
    }

    public List<GameObject> GetTurrets()
    {
        for (int i = 0; i < turrets.Count; i++)
        {
            if (turrets[i] == null)
            {
                turrets.RemoveAt(i);
                i = (i - 1 < 0) ? 0 : --i;
            }
        }

        return turrets;
    }

    #endregion Gameplay Methods
}