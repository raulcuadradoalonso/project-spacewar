﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpsManager : MonoBehaviour
{
    [Header("PowerUps costs")]
    [SerializeField] private float bombCost;
    [SerializeField] private float turretBoostCost;

    GeneralManager gm;

    //PowerUps
    IPowerUp currentPowerUp;
    PowerUpBomb puBomb;
    PowerUpTurretBoost puTurretBoost;

    float currentCost;

    void Start()
    { 
        //References set up
        gm = GetComponent<GeneralManager>();
        puBomb = GetComponent<PowerUpBomb>();
        puTurretBoost = GetComponent<PowerUpTurretBoost>();

        currentPowerUp = null;
    }

    void Update()
    {    
        if (currentPowerUp != null && !currentPowerUp.AlreadyUsed())
        {
            currentPowerUp.UsePowerUp();

            //The state of alreadyUsed will change always after the UsePowerUp function,
            //when the player activates the powerup using the left-button of the mouse
            if (currentPowerUp.AlreadyUsed())
            {
                //Once the power-up has been used, apply the energy cost
                gm.payEnergyCost(currentCost);
                currentPowerUp = null;
            }
        }

        //Right click to cancel power up
        if (Input.GetMouseButtonDown(1))
        {
            resetStatus();
        }
    }

    void resetStatus()
    {
        if (currentPowerUp != null)
        {
            currentPowerUp.CancelPowerUp();
        }

        currentPowerUp = null;
    }

    void resetUI()
    {

    }

    public void EnableDropBomb()
    {
        if (!gm.IsAffordable(bombCost)) { return; }

        resetStatus();

        //Update power up
        currentPowerUp = puBomb;

        //Enable power up
        currentPowerUp.EnablePowerUp();

        //Disable camera turret selector
        gm.disableCameraTurretSelector();

        //Store the cost of the power up (applied only if the player actually uses it)
        currentCost = bombCost;

        //TO DO: Highlight button
    }    

    public void EnableBoostTurret()
    {
        if (!gm.IsAffordable(turretBoostCost)) { return; }

        resetStatus();

        //Update power up
        currentPowerUp = puTurretBoost;

        //Enable power up
        currentPowerUp.EnablePowerUp();

        //Disable camera turret selector
        gm.disableCameraTurretSelector();

        //Store the cost of the power up (applied only if the player actually uses it)
        currentCost = turretBoostCost;

        //TO DO: Highlight button
    }
}
