﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1 : LevelInterface
{
    public float timeForWave1;
    public float timeForWave2;
    public float timeForWave3;

    protected override IEnumerator runBattle()
    {
        yield return new WaitForSeconds(timeForWave1);

        //Spawn wave 1

        //Normales
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[0]));

        //Capital
        currentEnemyInstances.Add(CreateCluster(enemyTypes[1], spawnPoints[1]));

        //Kamikaze
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[4]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[3]));

        yield return new WaitForSeconds(timeForWave2);

        //Spawn wave 2

        //Normales
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[0]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[2]));

        //Capital
        currentEnemyInstances.Add(CreateCluster(enemyTypes[1], spawnPoints[3]));

        //Kamikaze
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[4]));

        yield return new WaitForSeconds(timeForWave3);

        //Spawn wave 3

        //Normales
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[2]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[3]));

        //Capital

        //Kamikaze
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[0]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[1]));

        //And so on...

        //End of spawner
        spawning = false;
    }
}