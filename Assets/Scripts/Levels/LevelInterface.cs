﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LevelInterface : MonoBehaviour
{
    [SerializeField] protected GameObject[] spawnPoints;
    [SerializeField] protected List<GameObject> enemyTypes;
    [SerializeField] protected List<GameObject> currentEnemyInstances;

    [SerializeField] protected Commander commander;
    [SerializeField] protected GameObject megamind;

    protected bool spawning;

    public void EnableLevel()
    {
        spawning = true;
        StartCoroutine(runBattle());
    }

    public void DisableLevel()
    {
        StopAllCoroutines();
    }

    public bool IsSpawning()
    {
        return spawning;
    }

    protected GameObject CreateCluster(GameObject cluster, GameObject spawnPoint)
    {
        GameObject clusterCopy = Instantiate(cluster, spawnPoint.transform.position, Quaternion.identity);
        clusterCopy.SetActive(true);
        commander.AddCluster(clusterCopy.GetComponent<Vehicle>());

        AddObstacles(clusterCopy);

        //megamind.GetComponent<Cluster>().obstacles.Add(clusterCopy.GetComponent<Obstacle>());
        return clusterCopy;
    }

    private void AddObstacles(GameObject clusterGameObject)
    {
        Cluster cluster = clusterGameObject.GetComponent<Cluster>();
        if (cluster != null)
        {
            foreach (GameObject enemyInstance in currentEnemyInstances)
            {
                if (enemyInstance != null)
                {
                    Obstacle otherObstacle = enemyInstance.GetComponent<Obstacle>();
                    if (otherObstacle != null)
                    {
                        cluster.AddObstacle(otherObstacle);
                    }
                }
            }
        }

        Obstacle obstacle = clusterGameObject.GetComponent<Obstacle>();
        if (obstacle != null)
        {
            foreach (GameObject enemyInstance in currentEnemyInstances)
            {
                if (enemyInstance != null)
                {
                    Cluster otherCluster = enemyInstance.GetComponent<Cluster>();
                    if (otherCluster != null)
                    {
                        otherCluster.AddObstacle(obstacle);
                    }
                }
            }
        }
    }

    public int CheckNumberClusters()
    {
        for (int i = 0; i < currentEnemyInstances.Count; i++)
        {
            if (currentEnemyInstances[i] == null)
            {
                currentEnemyInstances.RemoveAt(i);
                i = (i - 1 < 0) ? 0 : --i;
            }
        }

        return currentEnemyInstances.Count;
    }

    protected abstract IEnumerator runBattle();
}