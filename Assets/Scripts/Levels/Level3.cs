﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level3 : LevelInterface
{
    public float timeForWave1;
    public float timeForWave2;
    public float timeForWave3;
    public float timeForWave4;

    protected override IEnumerator runBattle()
    {
        yield return new WaitForSeconds(timeForWave1);

        //Spawn wave 1

        //Normales
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[0]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[2]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[3]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[5]));

        //Capital
        currentEnemyInstances.Add(CreateCluster(enemyTypes[1], spawnPoints[1]));

        //Kamikaze

        yield return new WaitForSeconds(timeForWave2);

        //Spawn wave 2

        //Normales

        //Capital
        currentEnemyInstances.Add(CreateCluster(enemyTypes[1], spawnPoints[1]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[1], spawnPoints[4]));

        //Kamikaze
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[0]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[2]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[3]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[5]));

        yield return new WaitForSeconds(timeForWave3);

        //Spawn wave 3

        //Normales
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[1]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[4]));

        //Capital

        //Kamikaze

        yield return new WaitForSeconds(timeForWave4);

        //Spawn wave 4

        //Normales
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[0]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[0], spawnPoints[3]));

        //Capital
        currentEnemyInstances.Add(CreateCluster(enemyTypes[1], spawnPoints[4]));

        //Kamikaze
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[0]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[2]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[3]));
        currentEnemyInstances.Add(CreateCluster(enemyTypes[2], spawnPoints[5]));

        //And so on...

        //End of spawner
        spawning = false;
    }
}