﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenuScript : MonoBehaviour
{
    [Header("Canvas References")]
    public GameObject button_holder_canvas_GO_reference;

    public GameObject fade_to_scene_GO_reference;
    public GameObject canvas_GO_reference;

    public string sceneNameMainMenu = "";
    private bool animStarted;
    private string sceneToLoad;
    private float animationTimer;
    private Image fade_to_scene_Image_reference;

    // Start is called before the first frame update
    private void Start()
    {
        ChangeState(0);
        animationTimer = 0.0f;
        animStarted = false;
        fade_to_scene_Image_reference = fade_to_scene_GO_reference.GetComponent<Image>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (animStarted)
        {
            animationTimer += Time.deltaTime * 0.75f;
            fade_to_scene_Image_reference.color = new Color(0.0f, 0.0f, 0.0f, animationTimer);
            if (animationTimer >= 1.0f)
            {
                SceneManager.LoadScene(sceneToLoad);
            }
        }
        else if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 0.0f)
            {
                ResumeGame();
            }
            else
            {
                ChangeState(2); // Pause
            }
        }
    }

    public void ChangeToMainMenu()
    {
        ChangeState(1);
        sceneToLoad = sceneNameMainMenu;
        animStarted = true;

        // print("new Scene load!!");
    }

    public void ResumeGame()
    {
        ChangeState(3);
    }

    public void ChangeState(int newState)
    {
        switch (newState)
        {
            //0: estado inicial
            case 0:
                {
                    canvas_GO_reference.SetActive(false);
                    button_holder_canvas_GO_reference.SetActive(false);
                    fade_to_scene_GO_reference.SetActive(false);
                    break;
                }
            //1: estado animacion
            case 1:
                {
                    Time.timeScale = 1.0f;
                    button_holder_canvas_GO_reference.SetActive(false);
                    fade_to_scene_GO_reference.SetActive(true);
                    break;
                }
            //2: estado pausado
            case 2:
                {
                    Time.timeScale = 0.0f;
                    canvas_GO_reference.SetActive(true);
                    button_holder_canvas_GO_reference.SetActive(true);
                    fade_to_scene_GO_reference.SetActive(false);
                    break;
                }
            //3: UNPAUSE THIS
            case 3:
                {
                    Time.timeScale = 1.0f;
                    canvas_GO_reference.SetActive(false);
                    button_holder_canvas_GO_reference.SetActive(false);
                    fade_to_scene_GO_reference.SetActive(false);
                    break;
                }
            default:
                {
                    Debug.Log("you should not be here u piece of shit");
                    break;
                }
        }
    }
}