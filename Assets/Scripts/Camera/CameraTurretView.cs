﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTurretView : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] float speedH = 2.0f;
    [SerializeField] float speedV = 2.0f;
    [SerializeField] float maxYaw = 75;
    [SerializeField] float lookUpLimit = -20;
    [SerializeField] float lookDownLimit = 10; //Sign is positive in lower pitch cause x > 0 means look down

    float yaw = 0.0f;
    float pitch = 0.0f;

    float initialYRotation;
    float initialXRotation;

    Transform t;

    void Awake()
    {
        t = transform;
    }

    void Update()
    {
        getMouseInput();
    }

    void getMouseInput()
    {
        yaw += speedH * Input.GetAxis("Mouse X");
        pitch += speedV * Input.GetAxis("Mouse Y");

        if (Mathf.Abs(yaw) > maxYaw)
        {
            yaw = Mathf.Sign(yaw) * maxYaw;
        }

        if (pitch < lookUpLimit)
        {
            pitch = lookUpLimit;
        }
        else if (pitch > lookDownLimit)
        {
            pitch = lookDownLimit;
        }

        t.eulerAngles = new Vector3(initialXRotation - pitch, initialYRotation + yaw, t.eulerAngles.z);
    }

    void initTurretCameraMovement()
    {
        initialYRotation = t.eulerAngles.y;
        initialXRotation = t.eulerAngles.x;
    }

    void OnEnable()
    {
        initTurretCameraMovement();
        yaw = 0;
        pitch = 0;
    }

    void OnDisable()
    {

    }
}
