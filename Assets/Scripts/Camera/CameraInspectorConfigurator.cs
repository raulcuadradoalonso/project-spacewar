﻿using UnityEngine;
using System.Collections;
using UnityEditor;

#if UNITY_EDITOR
[CustomEditor(typeof(CameraBoundaries))]
public class CameraInspectorConfigurator : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CameraBoundaries cameraBoundariesConfig = (CameraBoundaries)target;

        if (GUILayout.Button("Set Left"))
        {
            cameraBoundariesConfig.SetUpLeft();
        }

        if (GUILayout.Button("Set Right"))
        {
            cameraBoundariesConfig.SetUpRight();
        }

        if (GUILayout.Button("Set Top"))
        {
            cameraBoundariesConfig.SetUpTop();
        }

        if (GUILayout.Button("Set Bottom"))
        {
            cameraBoundariesConfig.SetUpBottom();
        }
    }
}
#endif
