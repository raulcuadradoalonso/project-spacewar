﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTurretPositioner : MonoBehaviour
{
    [Header("Settings")]
    [Header("Turret costs")]
    [SerializeField] private float basicTurretCost = 10.0f;

    [Header("Materials")]
    [SerializeField] private Material transparentMaterial;
    [SerializeField] private Color colorAvoidSetting;

    [Header("References")]
    [SerializeField] private GeneralManager gm;
    [SerializeField] List<AudioClip> turretLandingSounds;

    private Camera cam;
    private CameraManager gcm;

    //References for the current turret that is going to be set
    private GameObject currentTurretPrefab;
    private TurretController currentTurretController;
    private TurretCollisionDetection currentTurretCollisionDetector;
    private Material turretMaterial;

    //For the blinking animation while turret is being set (see updateAlpha() function)
    private const float alphaLowerLimit = 0.55f;
    private const float alphaUpperLimit = 0.95f;
    private float currentAlpha = alphaUpperLimit;
    private const float alphaStep = 0.007f;
    private bool shouldDecrement = true;    

    private void Start()
    {
        cam = GetComponentInChildren<Camera>();
        gcm = GetComponent<CameraManager>();

        currentTurretPrefab = null;
    }

    private void Update()
    {
        if (currentTurretPrefab != null)
        {
            //Right-button click disables the selection
            if (Input.GetMouseButtonDown(1) && currentTurretPrefab != null)
            {
                Destroy(currentTurretPrefab);
            }

            //Update turret alpha (just a basic procedural animation)
            updateAlpha();

            //Check if the mouse is over a selectable area to set the turret
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            bool canBePositioned = currentTurretCollisionDetector.CanBeSet();
            if (!canBePositioned)
            {
                //Turret cannot be positioned over another turret!
                Color auxColor = colorAvoidSetting;
                auxColor.a = currentAlpha;
                currentTurretController.ChangeOutlineAndMainColor(auxColor, new Color(0, 0, 0, currentAlpha));
            }
            else
            {
                Color auxColor = currentTurretController.GetSelectedColor();
                auxColor.a = currentAlpha;
                currentTurretController.ChangeOutlineAndMainColor(auxColor, new Color(0, 0, 0, currentAlpha));
            }

            //While the player moves the mouse, he can see how the turret will be positioned
            if (Physics.Raycast(ray, out hit, 5000f, LayerMask.GetMask("AsteroidTurretRing")))
            {
                TurretRing currentRing = hit.collider.gameObject.GetComponent<TurretRing>();
                float ringRadius = (currentTurretController.GetTurretType() == TurretController.TURRET_TYPE.BASIC) ? currentRing.getBasicRadius() : currentRing.getHeavyRadius();
                float ringHeightOffset = (currentTurretController.GetTurretType() == TurretController.TURRET_TYPE.BASIC) ? currentRing.getBasicHeightOffset() : currentRing.getHeayHeightOffset();

                //----- Calculate the forward vector of the turret
                Vector3 impactPoint = hit.point;
                Vector3 asteroidCenter = hit.collider.gameObject.transform.position;

                //Fix the impact point to be at the same altitude than the asteroid center
                impactPoint.y = asteroidCenter.y;

                Vector3 turretForwardDirection = (impactPoint - asteroidCenter).normalized;
                currentTurretPrefab.transform.forward = -turretForwardDirection;

                //----- Calculate position of the turret
                asteroidCenter.y += ringHeightOffset;
                Vector3 newTurretPosition = asteroidCenter + turretForwardDirection * ringRadius;
                currentTurretPrefab.transform.position = newTurretPosition;

                //----- Show the object
                currentTurretPrefab.SetActive(true);

                //---- Set the turret
                if (canBePositioned) { setTurret(currentRing.getPlanet()); }
            }
            //If the mouse is not over any selectable area, we hide the auxiliar turret
            //that shows the player the final position of the turret
            else
            {
                //If the turret disappears, reset current alpha to 1
                currentAlpha = 1;

                //Set turret count to 0 because not triggering collider exit
                currentTurretCollisionDetector.ResetDetection();

                currentTurretPrefab.SetActive(false);
            }
        }
    }

    private void setTurret(GameObject planet)
    {
        if (Input.GetMouseButtonDown(0) && currentTurretPrefab != null)
        {
            //Play a sound
            currentTurretController.GetComponent<AudioSource>().PlayOneShot(turretLandingSounds[Random.Range(0, turretLandingSounds.Count)]);

            //Pay the energy cost
            gm.payEnergyCost(basicTurretCost);

            //Change turret color to default
            currentTurretController.ChangeOutlineAndMainColor(currentTurretController.GetDefaultColor(), Color.black);

            //Enable turret base layer
            currentTurretCollisionDetector.gameObject.layer = LayerMask.NameToLayer("ManualTurret");

            //Asign the planet to the turret
            currentTurretController.SetPlanet(planet);

            //Destroy turret collision detecter (we won't need it anymore)
            Destroy(currentTurretCollisionDetector);

            //Reset the material
            currentTurretController.SetMaterial(turretMaterial);            

            //Add the turret to the list
            gm.AddTurret(currentTurretPrefab);

            //Unbind the turret
            currentTurretPrefab = null;
            currentTurretController = null;

            gm.EnableReadyButton();            
        }
    }

    //Scripted animation of the turret blinking while it's being set
    private void updateAlpha()
    {
        if (shouldDecrement)
        {
            currentAlpha -= alphaStep;

            if (currentAlpha < alphaLowerLimit)
            {
                currentAlpha = alphaLowerLimit;
                shouldDecrement = false;
            }
        }
        else
        {
            currentAlpha += alphaStep;

            if (currentAlpha > alphaUpperLimit)
            {
                currentAlpha = alphaUpperLimit;
                shouldDecrement = true;
            }
        }

        Color auxColor = currentTurretController.GetCurrentColor();
        currentTurretController.ChangeOutlineAndMainColor(new Color(auxColor.r, auxColor.g, auxColor.b, currentAlpha), new Color(0, 0, 0, currentAlpha));
    }

    public void OnClickTurretPrefab(GameObject prefab)
    {
        //If this is true, it means that the player already clicked the button, but he didnt set up the turret,
        if (currentTurretPrefab != null)
        {
            Destroy(currentTurretPrefab.gameObject);
        }

        if (gm.IsAffordable(basicTurretCost))
        {
            //Assignments
            currentTurretPrefab = Instantiate(prefab);
            currentTurretController = currentTurretPrefab.GetComponent<TurretController>();
            currentTurretCollisionDetector = currentTurretPrefab.transform.Find("Turret base").gameObject.GetComponent<TurretCollisionDetection>();
            turretMaterial = currentTurretController.GetMaterial();

            currentTurretPrefab.SetActive(false);

            //Change turret material
            currentTurretController.SetMaterial(transparentMaterial);

            //Change turret color to 'beingPositioned' color
            currentTurretController.ChangeOutlineAndMainColor(currentTurretController.GetHighlightColor(), Color.black);

            //Disable the layer of the children to avoid the raycast from hitting it, because if not the raycast would always hit the turret, since it is positioned wherever the mouse is
            //Layer 0 is the default layer
            currentTurretCollisionDetector.gameObject.layer = 0;
            currentTurretCollisionDetector.enabled = true;
        }
    }

    public void OnDisable()
    {
        if (currentTurretPrefab != null)
        {
            Destroy(currentTurretPrefab);
        }
    }
}