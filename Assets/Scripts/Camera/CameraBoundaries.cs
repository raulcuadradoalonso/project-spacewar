﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBoundaries : MonoBehaviour
{

    public Transform left;
    public Transform right;
    public Transform top;
    public Transform bottom;

    public void SetUpLeft()
    {
        left.position = Camera.main.transform.position;
    }

    public void SetUpRight()
    {
        right.position = Camera.main.transform.position;
    }

    public void SetUpTop()
    {
        top.position = Camera.main.transform.position;
    }

    public void SetUpBottom()
    {
        bottom.position = Camera.main.transform.position;
    }

    public Vector3 GetLeftBoundary()
    {
        return left.position;
    }

    public Vector3 GetRightBoundary()
    {
        return right.position;
    }

    public Vector3 GetTopBoundary()
    {
        return top.position;
    }

    public Vector3 GetBottomBoundary()
    {
        return bottom.position;
    }
}
