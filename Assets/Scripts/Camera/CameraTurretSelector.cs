﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTurretSelector : MonoBehaviour
{

    TurretController targetTurret;
    Camera cam;

    void Start()
    {
        targetTurret = null;
        cam = Camera.main;
    }

    public TurretController SelectTurret(bool isForBoost, float transitionTime)
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 5000f, LayerMask.GetMask("ManualTurret")))
        {
            //If so, check if it's the first time the mouse is over it
            TurretController auxTargetTurret = hit.collider.gameObject.GetComponentInParent<TurretController>();

            if (!auxTargetTurret.IsTurretBoosted())
            {
                //If it's the first time that the mouse is over this turret, highlight it
                if (auxTargetTurret != targetTurret)
                {
                    //if target turret is not null we must turn it back to his deafult color before
                    if (targetTurret != null)
                    {
                        Color targetColor = targetTurret.GetDefaultColor();

                        targetTurret.turretColorTransition(targetColor, transitionTime);
                    }

                    targetTurret = auxTargetTurret;

                    Color highlight = (isForBoost) ? targetTurret.GetBoostedTurretHighlight() : targetTurret.GetHighlightColor();

                    targetTurret.turretColorTransition(highlight, transitionTime);
                }
            }

            return targetTurret;
        }
        else
        {
            //If the mouse goes away from the turret, change its color to default again
            if (targetTurret != null && !targetTurret.IsTurretBoosted())
            {
                Color targetColor = targetTurret.GetDefaultColor();

                targetTurret.turretColorTransition(targetColor, transitionTime * 0.1f);
                targetTurret = null;
            }
        }

        return null;
    }
}