﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.Tween;

public class CameraManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Texture2D mousePointer;

    [SerializeField] private GeneralManager gm;
    [SerializeField] private CanvasCollector canvasCollector;
    [SerializeField] private CameraBoundaries camBoundaries;

    [Header("Movement settings")]
    [SerializeField] private float mouseSpeed = 1.0f;

    [SerializeField] private int horizontalOffset = 50;
    [SerializeField] private int verticalOffset = 50;
    [SerializeField] private float cameraRefSpeed = 1.0f;
    [SerializeField] private float cameraSmoothSpeed = 1.0f;
    [SerializeField] private float maxZoomRange = 40f;
    [SerializeField] private float zoomAmount = 1f;

    [Header("Transition settings")]
    [SerializeField] private float defaultTransitionTime;

    //Mouse state variables
    private bool mouseEnabled;

    private bool positioningTurrets;

    //Transition variables
    private bool movementFinished = true;

    private bool rotationFinished = true;
    private CameraTurretSelector turretSelector;
    private TurretController targetTurretController;
    private CameraTurretView mt_camMovement;
    private GameObject currentPlanet;

    //Last positions of the camera in the general view to restore it after being in a turret
    private Vector3 lastPosition;

    private Quaternion lastRotation;

    //Timer to check if a transition is stopped at some point before it ends to create the new transition with an slower transition time
    private float auxTransitionTimer;

    private float transitionTime;

    private Camera cam;
    private Transform t;

    private Vector3 camRefPosition;
    private Vector3 camDefaultPosition;
    private float currentZoom = 0;

    //Status
    private bool turretSelectorEnabled = true;

    private enum CAMERA_STATUS
    {
        GENERAl_VIEW,
        TURRET_VIEW
    }

    private CAMERA_STATUS cameraStatus;

    private void Awake()
    {
        //Init references
        t = transform;
        mt_camMovement = GetComponent<CameraTurretView>();
        cam = GetComponentInChildren<Camera>();
        turretSelector = gm.gameObject.GetComponent<CameraTurretSelector>();

        //Init mouse state
        mouseEnabled = true;
        positioningTurrets = true;

        //Transition settings
        auxTransitionTimer = 0;
        transitionTime = defaultTransitionTime;

        //Movement variables
        currentZoom = 0;
        camRefPosition = t.position;
        camDefaultPosition = t.position;

        //Initial status
        cameraStatus = CAMERA_STATUS.GENERAl_VIEW;
    }

    private void Update()
    {
        switch (cameraStatus)
        {
            case CAMERA_STATUS.GENERAl_VIEW:

                if (mouseEnabled)
                {
                    //Check if the mouse is in the boundaries of the screen to move the camera
                    moveCamera();

                    if (!positioningTurrets && turretSelectorEnabled)
                    {
                        //Track if the player clicks on a turret to control it manually
                        cameraTurretSelector();
                    }
                }
                break;

            case CAMERA_STATUS.TURRET_VIEW:

                //Check if the turret could be null (destroyed)
                checkTurretStatus();
                //Check if the player wants to come back to the general view
                cameraToGeneral();
                break;

            default:

                Debug.LogError("ERROR: Camera status reached default!");
                break;
        }

        //Auxiliar transition timer will be enabled whenever a transition is being performed.
        //If the transition stops for some reason, the time to make the transition backwards will use the
        //auxiliar timer instead of the default time to make it faster
        if (!movementFinished && !rotationFinished)
        {
            auxTransitionTimer += Time.deltaTime;
        }
        else
        {
            auxTransitionTimer = 0;
        }
    }

    private void moveCamera()
    {
        Vector3 mousePosition = Input.mousePosition;

        //Camera reference position is updated first
        if ((mousePosition.x > Screen.width - horizontalOffset) && t.position.x < camBoundaries.GetRightBoundary().x)
        {
            //Move camera to the right
            camRefPosition += cameraRefSpeed * new Vector3(1, 0, 0);
        }
        else if ((mousePosition.x < horizontalOffset) && t.position.x > camBoundaries.GetLeftBoundary().x)
        {
            //move camera to the left
            camRefPosition -= cameraRefSpeed * new Vector3(1, 0, 0);
        }

        if ((mousePosition.y > Screen.height - verticalOffset) && t.position.z < camBoundaries.GetTopBoundary().z)
        {
            //Move camera forward
            camRefPosition += cameraRefSpeed * new Vector3(0, 0, 1);
        }
        else if ((mousePosition.y < verticalOffset) && t.position.z > camBoundaries.GetBottomBoundary().z)
        {
            //Move camera backward
            camRefPosition -= cameraRefSpeed * new Vector3(0, 0, 1);
        }

        zoomCamera();

        //Then the camera actually moves following smoothly the reference position
        Vector3 direction = (camRefPosition - t.position).normalized;
        float distance = Vector3.Distance(camRefPosition, t.position);

        t.position += direction * distance * cameraSmoothSpeed * Time.deltaTime;
    }

    private void zoomCamera()
    {
        if (Input.mouseScrollDelta.y > 0)
        {
            currentZoom += zoomAmount * Time.deltaTime;
        }
        else if (Input.mouseScrollDelta.y < 0)
        {
            currentZoom -= zoomAmount * Time.deltaTime;
        }

        currentZoom = Mathf.Clamp(currentZoom, 0, maxZoomRange);

        camRefPosition.y = camDefaultPosition.y - currentZoom;
    }

    private void cameraToGeneral()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StopAllCoroutines();

            //Enable general view and UI
            cameraStatus = CAMERA_STATUS.GENERAl_VIEW;
            gm.SecondPhaseUIStatus(true);

            //Disable current turret
            canvasCollector.ImageReticle.gameObject.SetActive(false);
            targetTurretController.DisableManualTurret();
            mt_camMovement.enabled = false;

            //---------Transition to general view
            Vector3 targetPos = lastPosition;
            Quaternion targetRot = lastRotation;
            cameraTransition(targetPos, targetRot);

            //---------Color transition to general view
            if (!targetTurretController.IsTurretBoosted()) { targetTurretController.turretColorTransition(targetTurretController.GetDefaultColor(), transitionTime); }
            //---------Outline width transition
            targetTurretController.turretOutlineTransition(targetTurretController.GetDefaultFarOutlineWidth(), transitionTime);

            StartCoroutine(waitToDisableTurret());
        }
    }

    private void cameraTurretSelector()
    {
        //Check if the mouse is over a turret
        targetTurretController = turretSelector.SelectTurret(false, defaultTransitionTime * 0.1f);

        //If the player clicks the left button, start transition to turret
        if (targetTurretController != null && Input.GetMouseButtonDown(0))
        {
            StopAllCoroutines();

            currentPlanet = targetTurretController.GetPlanet();

            //Disable general view and UI
            cameraStatus = CAMERA_STATUS.TURRET_VIEW;
            gm.SecondPhaseUIStatus(false);
            mouseEnabled = false;

            //Save last camera position (in general view) to come back later
            lastPosition = t.position;
            lastRotation = t.rotation;

            //Disable mouse
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            //--------- Transition to turret
            Vector3 targetPos = targetTurretController.GetCameraPositionReference().position;
            Quaternion targetRot = targetTurretController.GetCameraPositionReference().rotation;
            cameraTransition(targetPos, targetRot, true);

            //---------Color transition to turret
            if (!targetTurretController.IsTurretBoosted()) { targetTurretController.turretColorTransition(targetTurretController.GetSelectedColor(), transitionTime); }
            //---------Outline width transition
            targetTurretController.turretOutlineTransition(targetTurretController.GetDefaultNearOutlineWidth(), transitionTime);

            StartCoroutine(waitToDisableGeneralView());
        }
    }

    private void cameraTransition(Vector3 targetPos, Quaternion targetRot, bool disablePlanet = false)
    {
        //Reset bool movement trackers
        movementFinished = false;
        rotationFinished = false;

        //Check auxiliar timer
        //It's used for interrupted transitions, to make the new transition faster
        if (auxTransitionTimer == 0)
        {
            transitionTime = defaultTransitionTime;
        }
        else
        {
            transitionTime = auxTransitionTimer;
        }

        Vector3 currentPos = t.position;

        gameObject.Tween("Camera position", currentPos, targetPos, transitionTime, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            //Progress
            this.t.position = t.CurrentValue;
        }, (t) =>
        {
            //Completion
            movementFinished = true;
        });

        Quaternion currentRot = t.rotation;

        gameObject.Tween("Camera rotation", currentRot, targetRot, transitionTime, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            //Progress
            this.t.rotation = t.CurrentValue;

            //When moving the camera towards a turret, we need to disable the planet to avoid the outline to oclude the vision
            //since we are using front culling for the outline.
            if (disablePlanet && t.CurrentProgress >= 0.8f)
            {
                currentPlanet.GetComponent<MeshRenderer>().enabled = false;
            }
            //When going back to the general view, we have to set the planet active again, but this time at the beginning of the transition
            else if (!disablePlanet && t.CurrentProgress >= 0.2f)
            {
                currentPlanet.GetComponent<MeshRenderer>().enabled = true;
            }
        }, (t) =>
        {
            //Completion
            rotationFinished = true;
        });
    }

    //Scripts for turret control will not be enabled until the movement and rotation transitions are done
    private IEnumerator waitToDisableGeneralView()
    {
        while (!movementFinished && !rotationFinished)
        {
            yield return null;
        }

        //Enable turret script
        targetTurretController.EnableManualTurret();
        //Show reticle on the screen
        canvasCollector.ImageReticle.gameObject.SetActive(true);
        //Enable turret camera movement
        mt_camMovement.enabled = true;
    }

    //Scripts for general view will not be enabled until the movement and rotation transitions are done
    private IEnumerator waitToDisableTurret()
    {
        while (!movementFinished && !rotationFinished)
        {
            yield return null;
        }

        //Disable turret script
        targetTurretController = null;
        //Show virtual mouse pointer and enable it
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        mouseEnabled = true;
    }

    private void checkTurretStatus()
    {
        if (targetTurretController == null)
        {
            //Transition to general view again
            StopAllCoroutines();
            cameraStatus = CAMERA_STATUS.GENERAl_VIEW;

            //Enable general view and UI
            gm.SecondPhaseUIStatus(true);

            //Disable current turret
            canvasCollector.ImageReticle.gameObject.SetActive(false);
            mt_camMovement.enabled = false;

            //---------Transition to general view
            Vector3 targetPos = lastPosition;
            Quaternion targetRot = lastRotation;
            cameraTransition(targetPos, targetRot);

            StartCoroutine(waitToDisableTurret());
        }
    }

    private void OnDisable()
    {
        mt_camMovement.enabled = false;
    }

    public void EndPositioningTurrets()
    {
        positioningTurrets = false;
        GetComponent<CameraTurretPositioner>().enabled = false;
    }

    public void disableCameraTurretSelector()
    {
        turretSelectorEnabled = false;
    }

    public void enableCameraTurretSelector()
    {
        turretSelectorEnabled = true;
    }
}