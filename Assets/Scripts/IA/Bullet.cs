﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float destroyTime = 5f;
    [SerializeField] private bool isDestroyedByTime = true;

    [SerializeField] private float damage;

    private void Start()
    {
        if (isDestroyedByTime)
        {
            Invoke("Destroy", destroyTime);
        }
    }

    private void Destroy()
    {
        //Destroy(gameObject);
        gameObject.SetActive(false);
        CancelInvoke();
    }

    public float GetDamage()
    {
        return damage;
    }

    public void SetDamage(float dmg)
    {
        damage = dmg;
    }
}