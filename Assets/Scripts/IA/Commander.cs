﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Commander : MonoBehaviour
{
    public float distanceToTarget = 1;

    public List<GameObject> path;
    public List<Vehicle> vehicles;
    private List<int> vehiclesTarget;

    private void Start()
    {
        if (path == null || path.Count < 1)
        {
            Debug.LogWarning($"{gameObject.name} needs a path");
        }

        if (vehicles == null || vehicles.Count < 1)
        {
            Debug.LogWarning($"{gameObject.name} needs clusters");
        }

        vehiclesTarget = new List<int>();

        foreach (Vehicle vehicle in vehicles)
        {
            vehiclesTarget.Add(0);
        }
    }

    public void AddCluster(Vehicle vehicle)
    {
        vehicles.Add(vehicle);
        vehiclesTarget.Add(0);
    }

    private void Update()
    {
        UpdatePath();
    }

    private void UpdatePath()
    {
        float distanceToTargetTwo = distanceToTarget * distanceToTarget;

        for (int i = 0; i < vehicles.Count; i++)
        {
            if (vehicles[i] == null)
            {
                vehicles.RemoveAt(i);
                vehiclesTarget.RemoveAt(i);

                i = (i - 1 < 0) ? 0 : --i;
                break;
            }

            if ((path[vehiclesTarget[i]].transform.position - vehicles[i].transform.position).sqrMagnitude < distanceToTargetTwo)
            {
                if (vehiclesTarget[i] + 1 == path.Count)
                {
                    vehiclesTarget[i] = 0;
                }
                else
                {
                    vehiclesTarget[i]++;
                }

                //vehicles[i].target = path[vehiclesTarget[i]];
                vehicles[i].SetOriginalTarget(path[vehiclesTarget[i]]);
            }
        }
    }
}