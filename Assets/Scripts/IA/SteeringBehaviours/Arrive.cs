﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrive : SteeringBehaviour
{
    public float arriveDistance;

    public override Vector3 Steer()
    {
        Vector3 desired = sb.GetTarget().transform.position - transform.position;

        float distance = desired.magnitude;

        desired.Normalize();

        if (distance < arriveDistance)
        {
            float magnitude = Mathf2.Map(distance, 0, 100, 0, sb.maxSpeed);
            desired *= magnitude;
        }
        else
        {
            desired *= sb.maxSpeed;
        }

        Vector3 steer = desired - sb.velocity;
        steer = Vector3.ClampMagnitude(steer, sb.maxForce);

        return steer * weight;
        //ApplyForce(steer);
    }
}