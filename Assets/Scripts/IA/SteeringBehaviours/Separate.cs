﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Separate : SteeringBehaviour
{
    public float separation;

    public override Vector3 Steer()
    {
        Vector3 sum = new Vector3();
        int count = 0;

        if (sb.clusterFather == null)
        {
            Debug.LogWarning($"{gameObject.name} needs a father cluster");
            return Vector3.zero;
        }

        if (sb.clusterFather.obstacles == null || sb.clusterFather.vehicles.Count <= 1)
        {
            Debug.LogWarning($"{gameObject.name} is using separate but does not contain any neighbour");
            return Vector3.zero;
        }

        foreach (Vehicle other in sb.clusterFather.vehicles)
        {
            float distance = Vector3.Distance(transform.position, other.transform.position);
            if (distance > 0 && distance < separation)
            {
                Vector3 diff = transform.position - other.transform.position;
                diff.Normalize();
                diff /= distance;
                sum += diff;
                count++;
            }
        }

        if (count > 0)
        {
            sum /= count;
            sum.Normalize();
            sum *= sb.maxSpeed;
            Vector3 steer = sum - sb.velocity;
            steer = Vector3.ClampMagnitude(steer, sb.maxForce);
            return steer * weight;
            //ApplyForce(steer);
        }

        return Vector3.zero;
    }
}