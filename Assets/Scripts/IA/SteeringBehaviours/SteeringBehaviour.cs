﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SteeringBehaviour : MonoBehaviour
{
    protected Vehicle sb;
    [Range(0, 10)] public float weight = 0f;

    private void Awake()
    {
        sb = GetComponent<Vehicle>();
    }

    private void Start()
    {
        if (sb == null)
        {
            throw new System.Exception($"{gameObject.name} needs Steering Behaviour Component");
        }
    }

    abstract public Vector3 Steer();
}