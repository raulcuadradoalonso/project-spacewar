﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seek : SteeringBehaviour
{
    public override Vector3 Steer()
    {
        Vector3 desired = sb.GetTarget().transform.position - transform.position;
        desired.Normalize();
        desired *= sb.maxSpeed;

        Vector3 steer = desired - sb.velocity;
        steer = Vector3.ClampMagnitude(steer, sb.maxForce);

        return steer * weight;
        //ApplyForce(steer);
    }
}