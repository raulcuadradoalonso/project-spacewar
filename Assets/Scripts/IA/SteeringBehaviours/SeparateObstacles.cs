﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeparateObstacles : SteeringBehaviour
{
    public override Vector3 Steer()
    {
        Vector3 sum = new Vector3();
        int count = 0;

        if (sb.clusterFather == null)
        {
            Debug.LogWarning($"{gameObject.name} needs a father cluster");
            return Vector3.zero;
        }

        if (sb.clusterFather.obstacles == null || sb.clusterFather.obstacles.Count < 1)
        {
            Debug.LogWarning($"{gameObject.name} is using separate obstacles but does not contain any obstacle");
            return Vector3.zero;
        }

        for (int i = 0; i < sb.clusterFather.obstacles.Count; i++)
        {
            if (sb.clusterFather.obstacles[i] == null)
            {
                sb.clusterFather.obstacles.RemoveAt(i);
                i = (i - 1 < 0) ? 0 : --i;
            }
        }

        foreach (Obstacle other in sb.clusterFather.obstacles)
        {
            if (other == this)
            {
                break;
            }
            float distance = Vector3.Distance(transform.position, other.transform.position);
            if (distance > 0 && distance < other.radius)
            {
                Vector3 diff = transform.position - other.transform.position;
                diff.Normalize();
                diff /= distance;
                sum += diff;
                count++;
            }
        }

        if (count > 0)
        {
            sum /= count;
            sum.Normalize();
            sum *= sb.maxSpeed;
            Vector3 steer = sum - sb.velocity;
            steer = Vector3.ClampMagnitude(steer, sb.maxForce);
            return steer * weight;
            //ApplyForce(steer);
        }

        return Vector3.zero;
    }
}