﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Vehicle))]
public abstract class TargetChanger : MonoBehaviour
{
    protected Vehicle vehicle;
    [SerializeField] protected GameObject target;
    [SerializeField] private int priority = 1;

    private void Awake()
    {
        vehicle = GetComponent<Vehicle>();
    }

    private void Start()
    {
        if (vehicle == null)
        {
            throw new System.Exception($"{gameObject.name} needs Steering Behaviour Component");
        }
    }

    //This priority marks the IA behaviour when different events take place
    public int GetPriority()
    {
        return priority;
    }

    abstract public bool GetIsActive();

    abstract public GameObject GetEventTarget();
}