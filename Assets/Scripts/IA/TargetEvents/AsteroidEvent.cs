﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidEvent : TargetChanger
{
    [SerializeField] private int asteroidMask = 12;

    private RaycastHit hit;

    public override bool GetIsActive()
    {
        int layerMask = 1 << asteroidMask;
        Vector3 dir = vehicle.GetTarget().transform.position - transform.position;

        if (Physics.Raycast(transform.position, dir, out hit, Mathf.Infinity, layerMask) && (hit.distance < dir.magnitude))
        {
            Debug.DrawRay(transform.position, dir, Color.red, 10, false);
            //Debug.Log("Did Hit");
            return true;
        }
        else
        {
            Debug.DrawRay(transform.position, dir, Color.yellow, 0.3f, false);
            //Debug.Log("Did not Hit");
            return false;
        }
    }

    public override GameObject GetEventTarget()
    {
        //Vector3 dir = (vehicle.GetTarget().transform.position - hit.transform.position).normalized;
        //Vector3 newTargetPos = Vector3.Cross(dir, hit.transform.up);

        Vector3 dir = (hit.transform.position - transform.position).normalized;
        Vector3 crossProduct = Vector3.Cross(dir, hit.transform.up);

        float radius = hit.transform.GetComponent<SphereCollider>().radius * Mathf.Max(hit.transform.lossyScale.x, hit.transform.lossyScale.y, hit.transform.lossyScale.z);

        //Extra separation between obstacles
        float sep = GetComponent<Obstacle>() != null ? radius + GetComponent<Obstacle>().radius : radius * 1.2f;

        Vector3 newTargetPos;
        Vector3 newTargetPosA = hit.transform.position + (crossProduct * sep);
        Vector3 newTargetPosB = hit.transform.position - (crossProduct * sep);

        float distA2 = (newTargetPosA - vehicle.GetTarget().transform.position).sqrMagnitude;
        float distB2 = (newTargetPosB - vehicle.GetTarget().transform.position).sqrMagnitude;

        if (distA2 < distB2)
        {
            newTargetPos = newTargetPosA;
        }
        else
        {
            newTargetPos = newTargetPosB;
        }

        //TODO: CALCULATE A RANDOM ROTATION?

        return Instantiate(target, newTargetPos, new Quaternion());
    }
}