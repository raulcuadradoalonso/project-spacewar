﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightEvent : TargetChanger
{
    public bool hasEnteredFight = false;
    public GameObject turret;
    public float attackTurretWaitTime = 5f;

    private bool attackTurretReady = true;

    [SerializeField] private float distanceOffset = 20f;

    public override bool GetIsActive()
    {
        bool isActive = hasEnteredFight;
        hasEnteredFight = false;
        if (isActive && turret == null)
        {
            isActive = false;
        }
        return isActive;
    }

    public override GameObject GetEventTarget()
    {
        Vector3 newTargetPos = turret.transform.position;
        newTargetPos = newTargetPos - (turret.transform.forward * distanceOffset);

        return Instantiate(target, newTargetPos, new Quaternion());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ManualTurret" && other.transform.root == other.transform && (attackTurretReady || turret == null))
        {
            hasEnteredFight = true;
            turret = other.gameObject;
            attackTurretReady = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ManualTurret" && other.transform.root == other.transform && turret == other.gameObject)
        {
            Invoke("WaitForAttack", attackTurretWaitTime);
        }
    }

    private void WaitForAttack()
    {
        attackTurretReady = true;
    }
}