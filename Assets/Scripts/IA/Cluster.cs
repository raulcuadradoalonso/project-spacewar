﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cluster : MonoBehaviour
{
    [SerializeField] private int numVehicles;
    [SerializeField] private GameObject vehicle;
    //[SerializeField] private GameObject target;

    public List<Vehicle> vehicles;
    public List<Obstacle> obstacles;

    // Start is called before the first frame update
    private void Start()
    {
        vehicles = new List<Vehicle>(numVehicles);
        for (int i = 0; i < numVehicles; i++)
        {
            Vehicle newVehicle = Instantiate(vehicle, new Vector3(transform.position.x + Random.Range(-5f, 5f), 0, transform.position.z + Random.Range(-5f, 5f)), transform.rotation).GetComponent<Vehicle>();
            newVehicle.transform.parent = transform;
            //newVehicle.target = target;
            vehicles.Add(newVehicle);

            Cluster cluster = newVehicle.GetComponent<Cluster>();
            if (cluster != null)
            {
                cluster.obstacles = obstacles;
            }
        }

        foreach (Vehicle vehicle in vehicles)
        {
            vehicle.SetOriginalTarget(gameObject);
            vehicle.clusterFather = this;
        }
    }

    public void AddObstacle(Obstacle obstacle)
    {
        List<Cluster> clustersChildren = new List<Cluster>(GetComponentsInChildren<Cluster>());
        /*foreach (Cluster cluster in clustersChildren)
        {
            cluster.obstacles.Add(obstacle);
        }*/

        //for (int i = 0; i < clustersChildren.Count; i++)
        //{
        clustersChildren[0].obstacles.Add(obstacle);
        //}
    }

    public void DestroyDownUp(Vehicle vehicle)
    {
        vehicles.Remove(vehicle);
        if (vehicles.Count == 0)
        {
            Vehicle clusterVehicle = gameObject.GetComponent<Vehicle>();
            //Si es megamind o no tiene vehiculo
            if (clusterVehicle == null)
            {
                Obstacle obstacle = vehicle.GetComponent<Obstacle>();
                obstacles.Remove(obstacle);
                return;
            }
            clusterVehicle.DestroyDownUp();
            Destroy(gameObject);
        }
    }
}