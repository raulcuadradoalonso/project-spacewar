﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public float velocity = 3f;
    public float shootCooldown = 3f;
    public float cooldownThreshold = 0.15f;
    public int damage = 5;
    [SerializeField] private GameObject bulletParent;

    [SerializeField] private List<GameObject> shootTargets;

    private GameObject currentTarget;
    private float cooldown;

    //private bool keepShooting = false;
    private bool isReloading = true;

    private float timer;

    private void Start()
    {
        if (bulletParent == null)
        {
            bulletParent = GameObject.FindGameObjectWithTag("EnemyBulletPooler");
        }

        cooldown = Mathf.Max(shootCooldown + Random.Range(-cooldownThreshold, cooldownThreshold), 0f);
    }

    private void Update()
    {
        //If the spaceship has reloaded
        if (isReloading)
        {
            timer += Time.deltaTime;

            if (timer >= cooldown)
            {
                timer = 0;
                isReloading = false;
            }
        }
        else
        {
            if (currentTarget == null)
            {
                currentTarget = selectTarget();
            }
            //If there is one target in sight
            if (currentTarget != null)
            {
                ShootTarget();
            }
        }
    }

    private GameObject selectTarget()
    {
        float minDistance = float.PositiveInfinity;
        int targetIndex = -1;

        for (int i = 0; i < shootTargets.Count; i++)
        {
            if (shootTargets[i] == null)
            {
                shootTargets.RemoveAt(i);
                i = (i - 1 < 0) ? 0 : --i;
                break;
            }

            float currentDistance = Vector3.Distance(shootTargets[i].transform.position, transform.position);
            if (currentDistance < minDistance)
            {
                minDistance = currentDistance;
                targetIndex = i;
            }
        }

        if (targetIndex != -1)
        {
            return shootTargets[targetIndex];
        }

        return null;
    }

    /*private void ShootTarget()
    {
        if (!keepShooting)
        {
            return;
        }
        if (shootTarget == null)
        {
            //throw new System.Exception($"{gameObject.name} needs a target to shoot");
            return;
        }

        Vector3 dir = (shootTarget.transform.position - transform.position).normalized;
        dir = dir * velocity;
        GameObject newBullet = Instantiate(bullet, transform.position, new Quaternion());
        newBullet.GetComponent<Bullet>().SetDamage(damage);
        newBullet.GetComponent<Rigidbody>().velocity = dir;

        Invoke("ShootTarget", shootCooldown);
    }*/

    private void ShootTarget()
    {
        Vector3 dir = (currentTarget.transform.position - transform.position).normalized;
        dir = dir * velocity;
        GameObject newBullet = bulletParent.GetComponent<ObjectPooler>().GetPooledObject();
        newBullet.SetActive(true);
        newBullet.transform.position = transform.position;
        newBullet.transform.rotation = Quaternion.LookRotation(dir);
        //auxBullet.transform.SetParent(bulletParent.transform);
        newBullet.GetComponent<Bullet>().SetDamage(damage);
        newBullet.GetComponent<Rigidbody>().velocity = dir;

        isReloading = true;
        cooldown = Mathf.Max(shootCooldown + Random.Range(-cooldownThreshold, cooldownThreshold), 0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ManualTurret" && other.transform.root == other.transform)
        {
            shootTargets.Add(other.gameObject);
            //keepShooting = true;
            //Invoke("ShootTarget", shootTime);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ManualTurret" && other.transform.root == other.transform)
        {
            if (other.gameObject == currentTarget)
            {
                currentTarget = null;
            }
            shootTargets.Remove(other.gameObject);

            //keepShooting = false;
        }
    }
}