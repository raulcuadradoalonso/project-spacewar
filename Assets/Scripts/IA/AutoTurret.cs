﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoTurret : MonoBehaviour
{
    [SerializeField] private float damage;
    [SerializeField] private float shootCooldown = 1.0f;
    [SerializeField] private float velocity = 1.0f;
    [SerializeField] private float rotationSpeed = 5.0f;
    [SerializeField] private GameObject bulletParent;
    [SerializeField] private string bulletType;

    private bool reloading = true;

    private List<Vehicle> targetsInSight = new List<Vehicle>();
    private Vehicle currentTarget;

    private Transform[] turretBases;
    private Transform[] turretCannons;

    private Transform thisTransform;
    private TurretController tc;
    private TurretAnimations tAnimations;

    private float defaultDamage;
    private float defaultCooldown;
    private float defaultRotationSpeed;

    private float timer;

    //Cannons will shoot one by one
    private int currentIndexCannonToShoot = 0;

    private void Start()
    {
        thisTransform = transform;

        tc = GetComponent<TurretController>();
        tAnimations = GetComponent<TurretAnimations>();
        turretBases = tc.GetTurretBases();
        turretCannons = tc.GetTurretCannons();

        defaultCooldown = shootCooldown;
        defaultDamage = damage;
        defaultRotationSpeed = rotationSpeed;

        bulletParent = GameObject.FindGameObjectWithTag(bulletType);
    }

    private void Update()
    {
        //If the turret has reloaded
        if (reloading)
        {
            timer += Time.deltaTime;

            if (timer >= shootCooldown)
            {
                timer = 0;
                reloading = false;
            }
        }
        else
        {
            if (currentTarget == null)
            {
                currentTarget = selectTarget();
            }
            //If there is one target in sight
            if (currentTarget != null)
            {
                pointAtTarget();
            }
        }
    }

    private Vehicle selectTarget()
    {
        float minDistance = float.PositiveInfinity;
        int targetIndex = -1;

        for (int i = 0; i < targetsInSight.Count; i++)
        {
            if (targetsInSight[i] == null)
            {
                targetsInSight.RemoveAt(i);
                i = (i - 1 < 0) ? 0 : --i;
                break;
            }

            float currentDistance = Vector3.Distance(targetsInSight[i].transform.position, transform.position);
            if (currentDistance < minDistance)
            {
                minDistance = currentDistance;
                targetIndex = i;
            }
        }

        if (targetIndex != -1)
        {
            return targetsInSight[targetIndex];
        }

        return null;
    }

    private void pointAtTarget()
    {
        //Positions A
        float x0A = currentTarget.transform.position.x;
        float y0A = currentTarget.transform.position.y;
        float z0A = currentTarget.transform.position.z;

        //Positions B
        float x0B = transform.position.x;
        float y0B = transform.position.y;
        float z0B = transform.position.z;

        //Vel A
        float vx0A = currentTarget.velocity.x;
        float vy0A = currentTarget.velocity.y;
        float vz0A = currentTarget.velocity.z;

        //Dist
        float dx = x0A - x0B;
        float dy = y0A - y0B;
        float dz = z0A - z0B;

        float a = (velocity * velocity) - (vx0A * vx0A) - (vy0A * vy0A) - (vz0A * vz0A);
        float b = -2 * ((dx * vx0A) - (dy * vy0A) - (dz * vz0A));
        float c = -((dx * dx) + (dy * dy) + (dz * dz));

        float t = Mathf2.TwoGradEc(a, b, c);

        Vector3 nextPosition = currentTarget.transform.position + currentTarget.velocity * t;

        Vector3 targetPosition = nextPosition;
        Vector3 targetDirection = (targetPosition - thisTransform.position).normalized;
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);

        for (int i = 0; i < turretBases.Length; i++)
        {
            turretBases[i].rotation = Quaternion.Lerp(turretBases[i].rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }

        //Posibles ideas:
        //  - Independizar la animación de giro de los cañones (suavizar) del disparo en sí (aunque las balas salgan 'desviadas' del cañón, de lejos no se nota)
        //  - Disparar aunque el ángulo de disparo no sea aún el adecuado

        if (Mathf.Abs(Vector3.Angle(turretBases[0].forward, targetDirection)) < 0.1f)
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        /*
        for (int i = 0; i < turretCannons.Length; i++)
        {
            GameObject auxBullet = Instantiate(tc.GetBulletPrefab(), turretCannons[i].position, turretBases[0].rotation);
            auxBullet.transform.SetParent(thisTransform);
            auxBullet.GetComponent<Rigidbody>().velocity = turretBases[0].forward * velocity;
        }
        */

        //At this moment our turrets only have one base so we can use turretBases[0] directly to take the direction

        GameObject auxBullet = bulletParent.GetComponent<ObjectPooler>().GetPooledObject();
        auxBullet.SetActive(true);
        auxBullet.transform.position = turretCannons[currentIndexCannonToShoot].position;
        auxBullet.transform.rotation = turretBases[0].rotation;
        auxBullet.GetComponent<Rigidbody>().velocity = turretBases[0].forward * velocity;
        auxBullet.GetComponent<Bullet>().SetDamage(damage);

        tAnimations.cannonShootAnimation(currentIndexCannonToShoot, shootCooldown);

        currentIndexCannonToShoot = (currentIndexCannonToShoot == turretCannons.Length - 1) ? 0 : currentIndexCannonToShoot + 1;

        reloading = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Vehicle")
        {
            Vehicle vehicle = other.GetComponent<Vehicle>();
            targetsInSight.Add(vehicle);
            //vehicle.AddAutoTurret(this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Vehicle")
        {
            Vehicle auxVehicle = other.GetComponent<Vehicle>();

            if (auxVehicle == null)
            {
                currentTarget = null;
                return;
            }

            targetsInSight.Remove(auxVehicle);

            if (currentTarget == auxVehicle)
            {
                currentTarget = null;
            }
        }
    }

    public float GetDamage()
    {
        return damage;
    }

    public float GetCooldown()
    {
        return shootCooldown;
    }

    public float GetRotationSpeed()
    {
        return rotationSpeed;
    }

    public void SetDamage(float dmg)
    {
        damage = dmg;
    }

    public void SetCooldown(float cd)
    {
        shootCooldown = cd;
    }

    public void SetRotationSpeed(float rotation)
    {
        rotationSpeed = rotation;
    }

    public void SetDefaultDamage()
    {
        damage = defaultDamage;
    }

    public void SetDefaultCooldown()
    {
        shootCooldown = defaultCooldown;
    }

    public void SetDefaultRotationSpeed()
    {
        rotationSpeed = defaultRotationSpeed;
    }
}