﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Vehicle : MonoBehaviour
{
    private Rigidbody rb;
    private List<SteeringBehaviour> steeringBehaviours;
    public List<TargetChanger> targetChangers;

    public Vector3 velocity;
    public Vector3 acceleration;

    public Cluster clusterFather;
    [SerializeField] private GameObject target;
    private GameObject decidedTarget;
    public TargetChanger currentTargetChanger;

    public float maxForce;
    public float maxSpeed;
    public int life = 100;
    public float distanceToDecidedTarget = 2f;

    public GameObject impactPartSystem;
    public GameObject deathPartSystem;

    private float rotationSpeed = 5;

    private bool isDecidingTarget = false;

    private CompleteShield shield;

    #region Getters && Setters

    public GameObject GetTarget()
    {
        if (isDecidingTarget)
        {
            return decidedTarget;
        }

        return target;
    }

    public void SetOriginalTarget(GameObject target)
    {
        this.target = target;
    }

    #endregion Getters && Setters

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rb.interpolation = RigidbodyInterpolation.Interpolate;
        rb.useGravity = false;
        steeringBehaviours = new List<SteeringBehaviour>(GetComponents<SteeringBehaviour>());
        targetChangers = new List<TargetChanger>(GetComponents<TargetChanger>());
        shield = GetComponentInChildren<CompleteShield>();
    }

    private void Start()
    {
        velocity = Vector3.zero;
        acceleration = Vector3.zero;
        if (shield != null)
        {
            shield.MaxHealth = life;
            shield.currentHealth = life;
        }
    }

    private void Update()
    {
        //If it is completed, search for another one
        if (!isDecidingTarget)
        {
            TestTargetChange(1000);
        }
        //Check if the decided target has been completed
        else
        {
            UpdateDecidingState();
        }
    }

    private void UpdateDecidingState()
    {
        float distanceToDecidedTarget2 = distanceToDecidedTarget * distanceToDecidedTarget;
        if ((decidedTarget.transform.position - transform.position).sqrMagnitude < distanceToDecidedTarget2)
        {
            isDecidingTarget = false;
            Destroy(decidedTarget);
            decidedTarget = null;
            currentTargetChanger = null;
        }
        else
        {
            TestTargetChange(currentTargetChanger.GetPriority());
        }
    }

    private void TestTargetChange(int priority)
    {
        //currentTargetChanger = null;
        //float lastDecidedTargetDist2 = Mathf.Infinity;

        foreach (TargetChanger targetChanger in targetChangers)
        {
            int newPriority = targetChanger.GetPriority();
            if (newPriority < priority)
            {
                if (targetChanger.GetIsActive())
                {
                    Destroy(decidedTarget);
                    newPriority = priority;
                    currentTargetChanger = targetChanger;

                    decidedTarget = currentTargetChanger.GetEventTarget();
                    isDecidingTarget = true;
                    break;
                }
            }
        }
        /*
        if (currentTargetChanger != null)
        {
            decidedTarget = currentTargetChanger.GetEventTarget();
            isDecidingTarget = true;
        }
        */
    }

    private void FixedUpdate()
    {
        Vector3 steering = new Vector3();

        foreach (SteeringBehaviour steeringBehaviour in steeringBehaviours)
        {
            steering += steeringBehaviour.Steer();
            ApplyForce(steering);
        }

        ApplyVelocity();

        ApplyRotation();

        acceleration = Vector3.zero;
    }

    private void ApplyVelocity()
    {
        velocity += acceleration;
        velocity = Vector3.ClampMagnitude(velocity, maxSpeed);
        rb.MovePosition(transform.position + velocity * Time.deltaTime);
    }

    private void ApplyRotation()
    {
        //Look at velocity direction(can be changed in future)
        /*
        Vector3 eulerAngleVel = new Vector3(0, Vector3.Angle(transform.forward, velocity), 0);
        Quaternion deltaRotation = Quaternion.Euler(eulerAngleVel * Time.deltaTime);
        rb.MoveRotation(rb.rotation * deltaRotation);
        */

        Quaternion targetRotation = Quaternion.LookRotation(velocity.normalized, Vector3.up);
        targetRotation = Quaternion.Euler(targetRotation.eulerAngles.x, targetRotation.eulerAngles.y + 90, targetRotation.eulerAngles.z);
        rb.rotation = Quaternion.Lerp(rb.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }

    private void ApplyForce(Vector3 force)
    {
        acceleration += force;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "AllyBullet" && tag == "Vehicle")
        {
            Bullet turretBullet = other.GetComponent<Bullet>();
            if (turretBullet != null)
            {
                //Destroy(turretBullet.gameObject);
                if (life > 0 && impactPartSystem != null)
                {
                    //GameObject impactPS = Instantiate(impactPartSystem, GetComponent<Collider>().ClosestPoint(other.transform.position), Quaternion.Inverse(other.transform.rotation));
                    GameObject impactPS = Instantiate(impactPartSystem, transform.position, Quaternion.Inverse(other.transform.rotation));
                    Destroy(impactPS, 1f);
                }

                turretBullet.gameObject.SetActive(false);
                MakeDamage((int)turretBullet.GetDamage());
                if (shield != null)
                {
                    shield.AddDataPoint(other.transform.position);
                    shield.ReceiveDamage(turretBullet.GetDamage());
                }
            }
        }
    }

    public void MakeDamage(int damage)
    {
        life -= damage;

        if (life <= 0)
        {
            GameObject deathPS = Instantiate(deathPartSystem, transform.position, transform.rotation);
            Destroy(deathPS, 2f);
            DestroyDownUp();
        }
    }

    public void DestroyDownUp()
    {
        if (clusterFather != null)
        {
            clusterFather.DestroyDownUp(this);
            Destroy(gameObject);
        }
    }
}