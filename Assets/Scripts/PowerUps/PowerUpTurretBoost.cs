﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpTurretBoost : IPowerUp
{
    [Header ("Settings")]
    [SerializeField] float boostTime;
    [SerializeField] float boostCdMultiplier;
    [SerializeField] float boostDmgMultiplier;
    [SerializeField] float defaultTransitionTime = 0.15f;

    GeneralManager gm;
    CameraTurretSelector turretSelector;
    TurretController targetTurret;

    void Start()
    {
        alreadyUsed = false;

        gm = GetComponent<GeneralManager>();
        turretSelector = GetComponent<CameraTurretSelector>();
    }

    public override void EnablePowerUp()
    {
        alreadyUsed = false;
    }

    public override void UsePowerUp()
    {
        //Check if the mouse is over a turret
        targetTurret = turretSelector.SelectTurret(true, defaultTransitionTime);

        //Boost selected turret
        if (targetTurret != null && Input.GetMouseButtonDown(0))
        {
            alreadyUsed = true;

            targetTurret.turretColorTransition(targetTurret.GetBoostedTurretColor(), defaultTransitionTime);
            targetTurret.EnableBoostTurret(boostDmgMultiplier, boostCdMultiplier, boostTime);

            targetTurret = null;

            StartCoroutine(enableCameraTurretSelector());
        }
    }

    public override void CancelPowerUp()
    {
        targetTurret = null;
        StartCoroutine(enableCameraTurretSelector());
    }

    public override bool AlreadyUsed()
    {
        return alreadyUsed;
    }

    IEnumerator enableCameraTurretSelector()
    {
        yield return new WaitForSeconds(0.1f);

        gm.enableCameraTurretSelector();
    }
}
