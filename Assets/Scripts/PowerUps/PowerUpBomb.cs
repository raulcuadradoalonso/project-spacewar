﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpBomb : IPowerUp
{
    [Header("Bomb settings")]
    [SerializeField] private float bombScale;
    [SerializeField] AudioClip bombSound;

    [Header("Radius settings")]
    [SerializeField] private float radius;

    [SerializeField] private int numberOfRadiusElements;
    [SerializeField] private Vector3 radiusCubeScale;
    [SerializeField] private float rotationSpeed;

    [Header("References")]
    [SerializeField] private Material outlineOpaque;

    [SerializeField] private Material outlineTransparent;
    [SerializeField] private GameObject cubePrefab;

    private GeneralManager gm;

    private GameObject bombRadiusParent;
    private GameObject bomb;

    private GameObject horizontalRingParent;
    private GameObject verticalRingParent;

    private void Start()
    {
        alreadyUsed = false;

        gm = GetComponent<GeneralManager>();
        horizontalRingParent = new GameObject();
        verticalRingParent = new GameObject();

        createBomb();
    }

    //The radious of the bomb is showed by a circle made of small cubes
    private void createBomb()
    {
        //Bomb itself
        bomb = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        bomb.transform.localScale = bomb.transform.localScale * bombScale;
        bomb.GetComponent<MeshRenderer>().material = outlineOpaque;

        //Bomb radius
        bombRadiusParent = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        bombRadiusParent.transform.localScale = bombRadiusParent.transform.localScale * (radius * 2);
        bombRadiusParent.GetComponent<MeshRenderer>().material = outlineTransparent;
        SphereCollider bombCollider = bombRadiusParent.AddComponent<SphereCollider>();
        bombCollider.isTrigger = true;
        bombRadiusParent.AddComponent<Bomb>();

        //Middle horizontal radius ring
        createHorizontalBombRadius(horizontalRingParent.transform, 0);

        //Upper horizontal middle ring
        createHorizontalBombRadius(horizontalRingParent.transform, radius * 0.5f);

        //Lower horizontal middle ring
        createHorizontalBombRadius(horizontalRingParent.transform, -radius * 0.5f);

        //Middle vertical radius ring
        createVerticalBombRadius(verticalRingParent.transform, 0);

        //Right vertical radius ring
        createVerticalBombRadius(verticalRingParent.transform, radius * 0.5f);

        //Left vertical radius ring
        createVerticalBombRadius(verticalRingParent.transform, -radius * 0.5f);

        horizontalRingParent.transform.SetParent(bombRadiusParent.transform);
        verticalRingParent.transform.SetParent(bombRadiusParent.transform);

        bomb.SetActive(false);
        bombRadiusParent.SetActive(false);
    }

    private void createHorizontalBombRadius(Transform parent, float h)
    {
        float angleStep = 360.0f / (float)numberOfRadiusElements;
        float currentStep = 0;

        for (int i = 0; i < numberOfRadiusElements; i++)
        {
            GameObject auxObject = Instantiate(cubePrefab, new Vector3(0, 0, 0), Quaternion.identity);

            auxObject.transform.localScale = radiusCubeScale;
            auxObject.transform.Rotate(new Vector3(0.0f, currentStep, 0.0f));

            currentStep += angleStep;

            //If we have a displacement over the vertical axis
            float currentRadius = radius;
            if (h != 0)
            {
                currentRadius = Mathf.Sqrt(2 * radius * Mathf.Abs(h) - h * h);
                auxObject.transform.position = new Vector3(0.0f, h, 0.0f);
            }

            auxObject.transform.position += auxObject.transform.forward * currentRadius;
            auxObject.transform.SetParent(parent);

            auxObject.GetComponentInChildren<MeshRenderer>().material = outlineOpaque;
        }
    }

    private void createVerticalBombRadius(Transform parent, float h)
    {
        float angleStep = 360.0f / (float)numberOfRadiusElements;
        float currentStep = 0;

        for (int i = 0; i < numberOfRadiusElements; i++)
        {
            GameObject auxObject = Instantiate(cubePrefab, new Vector3(0, 0, 0), Quaternion.identity);

            auxObject.transform.localScale = radiusCubeScale;
            auxObject.transform.Rotate(new Vector3(0.0f, 0.0f, currentStep));

            currentStep += angleStep;

            //If we have a displacement over the vertical axis
            float currentRadius = radius;
            if (h != 0)
            {
                currentRadius = Mathf.Sqrt(2 * radius * Mathf.Abs(h) - h * h);
                auxObject.transform.position = new Vector3(0.0f, 0.0f, h);
            }

            auxObject.transform.position += auxObject.transform.up * currentRadius;
            auxObject.transform.SetParent(parent);

            auxObject.GetComponentInChildren<MeshRenderer>().material = outlineOpaque;
        }
    }

    public override void EnablePowerUp()
    {

        alreadyUsed = false;

        bombRadiusParent.SetActive(true);
        bomb.SetActive(true);

        //Set an initial rotation for the bomb (just for aesthetic purposes)
        bombRadiusParent.transform.rotation = Quaternion.Euler(0.0f, 45.0f, 0.0f);
    }

    public override void UsePowerUp()
    {
        //Show the bomb and its radious where the mouse is at a fixed plane altitude
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 5000f, LayerMask.GetMask("BasePlane")))
        {
            Vector3 bombPosition = hit.point;

            bombRadiusParent.transform.position = bombPosition;
            bomb.transform.position = bombPosition;
        }

        //Rotate the bomb radius (just visual effect)
        float step = rotationSpeed * Time.deltaTime;
        horizontalRingParent.transform.Rotate(new Vector3(0.0f, step, 0.0f));
        verticalRingParent.transform.Rotate(new Vector3(0.0f, 0.0f, step));
        bombRadiusParent.transform.Rotate(new Vector3(0.0f, step * 0.5f, 0.0f));

        //If the player presses left click, drop it
        if (Input.GetMouseButtonDown(0))
        {
            alreadyUsed = true;

            bombRadiusParent.GetComponent<Bomb>().Explode();

            bombRadiusParent.SetActive(false);
            bomb.SetActive(false);

            StartCoroutine(enableCameraTurretSelector());

            GetComponent<AudioSource>().PlayOneShot(bombSound);
        }
    }

    public override void CancelPowerUp()
    {
        bombRadiusParent.SetActive(false);
        bomb.SetActive(false);
        StartCoroutine(enableCameraTurretSelector());
    }

    public override bool AlreadyUsed()
    {
        return alreadyUsed;
    }

    private IEnumerator enableCameraTurretSelector()
    {
        yield return new WaitForSeconds(0.1f);

        gm.enableCameraTurretSelector();
    }
}