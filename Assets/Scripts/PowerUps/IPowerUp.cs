﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IPowerUp : MonoBehaviour
{
    [SerializeField] protected float cost;
    protected bool alreadyUsed;    

    public abstract void EnablePowerUp();
    public abstract void UsePowerUp();
    public abstract void CancelPowerUp();
    public abstract bool AlreadyUsed();
}
