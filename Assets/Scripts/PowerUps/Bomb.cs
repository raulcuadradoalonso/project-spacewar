﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    private List<Vehicle> vehicles;
    public int damage = 3000;

    private void Start()
    {
        vehicles = new List<Vehicle>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Vehicle")
        {
            vehicles.Add(other.GetComponent<Vehicle>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Vehicle")
        {
            vehicles.Remove(other.GetComponent<Vehicle>());
        }
    }

    public void Explode()
    {
        foreach (Vehicle vehicle in vehicles)
        {
            if (vehicle != null)
            {
                vehicle.MakeDamage(damage);
            }
        }

        vehicles.Clear();
    }
}