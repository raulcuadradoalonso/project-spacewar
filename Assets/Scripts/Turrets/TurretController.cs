﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;
using DigitalRuby.Tween;

public class TurretController : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Transform[] turretBases;
    [SerializeField] private Transform[] turretCannons;
    [SerializeField] private Transform camPosReference;
    [SerializeField] private GameObject[] meshes;
    [SerializeField] private GameObject bulletParent;
    [SerializeField] private string bulletType;
    private GameObject m_planet; //The reference is got through an script so it doesn't need to be serialized

    [Header("Turret settings")]
    [SerializeField] private float damage;
    [SerializeField] private float targetDistance;
    [SerializeField] private float cooldown;
    [SerializeField] private bool automatic;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float bulletSpeed;

    [Header("Material settings")]
    [SerializeField] private Color highlightColor;
    [SerializeField] private Color selectedColor;
    [SerializeField] private Color defaultColor;
    [SerializeField] private Color boostedTurretHightlight;
    [SerializeField] private Color boostedTurretColor;

    //Material outline width (changes with distance)
    [SerializeField] private float cameraNearOutlineWidth;
    [SerializeField] private float cameraFarOutlineWidth;

    [Header("Camera Settings")]
    public float Magnitude = 2f;
    public float Roughness = 10f;
    public float FadeOutTime = 5f;

    private float cooldownTimer;

    private bool isTurretManual;
    private bool hasShot = false;
    private bool canShoot = true;

    private Camera cam;
    private Transform thisTransform;
    private AutoTurret at;
    private TurretAnimations tAnimations;

    private bool turretBoosted = false;
    private float boostAuxTimer = 0;
    private float boostTime;

    //Default values
    private float defaultDamage;
    private float defaultCooldown;

    //Cannons will shoot one by one
    private int currentIndexCannonToShoot = 0;

    //Materials setup
    private Material[] meshesMaterials;
    private Color currentColor;
    private float currentOutlineWidth;

    public enum TURRET_TYPE
    {
        BASIC,
        HEAVY
    }

    [SerializeField] TURRET_TYPE currentTurretType;

    private void Awake()
    {
        cam = Camera.main;
        at = GetComponent<AutoTurret>();
        tAnimations = GetComponent<TurretAnimations>();

        thisTransform = transform;
        isTurretManual = false;
        currentColor = defaultColor;

        defaultDamage = damage;
        defaultCooldown = cooldown;

        //Create instance of the material to modify it locally
        setUpMaterials();
        currentOutlineWidth = cameraFarOutlineWidth;
    }

    private void Start()
    {
        bulletParent = GameObject.FindGameObjectWithTag(bulletType);
    }

    private void Update()
    {
        boostTurret();

        //Manual system
        if (isTurretManual)
        {
            pointAtTarget();

            playerInput();

            shootCooldown();
        }                
    }

    #region Shooting stuff

    private void pointAtTarget()
    {
        Vector3 target = cam.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, cam.nearClipPlane)) + cam.transform.forward * targetDistance;
        Vector3 targetDirection = (target - thisTransform.position).normalized;
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);

        for (int i = 0; i < turretBases.Length; i++)
        {
            //turretBases[i].LookAt(target);
            turretBases[i].rotation = Quaternion.Lerp(turretBases[i].rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }
    }

    private void playerInput()
    {
        if (!automatic && Input.GetMouseButtonDown(0))
        {
            shoot();            
        }
        else if (automatic && Input.GetMouseButton(0))
        {
            shoot();
        }
    }

    private void shoot()
    {
        if (canShoot)
        {
            hasShot = true;
            canShoot = false;

            CameraShaker.Instance.ShakeOnce(Magnitude, Roughness, 0, FadeOutTime);

            GameObject auxBullet = bulletParent.GetComponent<ObjectPooler>().GetPooledObject();
            auxBullet.SetActive(true);
            auxBullet.transform.position = turretCannons[currentIndexCannonToShoot].position;
            auxBullet.transform.rotation = turretBases[0].rotation;
            auxBullet.GetComponent<Rigidbody>().velocity = bulletSpeed * auxBullet.transform.forward;
            auxBullet.GetComponent<Bullet>().SetDamage(damage);

            tAnimations.cannonShootAnimation(currentIndexCannonToShoot, cooldown);

            currentIndexCannonToShoot = (currentIndexCannonToShoot == turretCannons.Length - 1) ? 0 : currentIndexCannonToShoot + 1;
        }
    }

    private void shootCooldown()
    {
        if (hasShot)
        {
            cooldownTimer += Time.deltaTime;

            if (cooldownTimer >= cooldown)
            {
                cooldownTimer = 0;
                canShoot = true;
                hasShot = false;
            }
        }
    }

    #endregion Shooting stuff

    #region Turret state management

    public void EnableManualTurret()
    {
        isTurretManual = true;
        at.enabled = false;
    }

    public void DisableManualTurret()
    {
        isTurretManual = false;
        at.enabled = true;
    }

    public void EnableBoostTurret(float dmg_multiplier, float cd_multiplier, float time)
    {
        //Boost stats
        damage *= dmg_multiplier;
        cooldown *= cd_multiplier;

        //Boost automatic turret too
        at.SetCooldown(cd_multiplier * at.GetCooldown());
        at.SetDamage(dmg_multiplier * at.GetDamage());

        //Fix for automatic turrets to shoot faster
        at.SetRotationSpeed(at.GetRotationSpeed() * 2);

        boostTime = time;
        boostAuxTimer = 0;

        turretBoosted = true;
    }

    private void unboostTurret()
    {
        damage = defaultDamage;
        cooldown = defaultCooldown;

        at.SetDefaultCooldown();
        at.SetDefaultDamage();
        at.SetDefaultRotationSpeed();
    }

    private void boostTurret()
    {
        //Boost
        if (turretBoosted)
        {
            boostAuxTimer += Time.deltaTime;
            //End boost
            if (boostAuxTimer >= boostTime)
            {
                boostAuxTimer = 0;
                turretBoosted = false;

                unboostTurret();
                if (isTurretManual) { turretColorTransition(GetSelectedColor(), 0.3f); }
                else                { turretColorTransition(GetDefaultColor(), 0.3f);  }
            }
        }
    }

    #endregion Turret state management

    #region Turret color management

    private void setUpMaterials()
    {
        meshesMaterials = new Material[meshes.Length];

        for (int i = 0; i < meshes.Length; i++)
        {
            Material auxMat = new Material(meshes[i].GetComponent<MeshRenderer>().material);
            meshes[i].GetComponent<MeshRenderer>().material = auxMat;
            meshesMaterials[i] = auxMat;
        }
    }

    public void ChangeTurretWiredOutlineColor(Color color)
    {
        for (int i = 0; i < meshesMaterials.Length; i++)
        {
            meshesMaterials[i].SetColor("_OutlineColor", color);
            meshesMaterials[i].SetColor("_WireColor", color);
        }

        currentColor = color;
    }

    public void ChangeOutlineAndMainColor(Color outlineColor, Color mainColor)
    {
        for (int i = 0; i < meshesMaterials.Length; i++)
        {            
            meshesMaterials[i].SetColor("_OutlineColor", outlineColor);
            meshesMaterials[i].SetColor("_Color", mainColor);
        }

        currentColor = outlineColor;
    }

    private void ChangeTurretOutlineWidth(float width)
    {
        for (int i = 0; i < meshesMaterials.Length; i++)
        {
            meshesMaterials[i].SetFloat("_OutlineWidth", width);
        }

        currentOutlineWidth = width;
    }

    public void turretColorTransition(Color targetColor, float time = 0.1f)
    {
        Color initialColor = GetCurrentColor();

        gameObject.Tween("Turret color " + gameObject.GetInstanceID(), initialColor, targetColor, time, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            //Progress
            ChangeTurretWiredOutlineColor(t.CurrentValue);
        });
    }

    public void turretOutlineTransition(float width, float time = 0.1f)
    {
        gameObject.Tween("Turret outline " + gameObject.GetInstanceID(), GetCurrentOutlineWidth(), width, time, TweenScaleFunctions.CubicEaseInOut, (t) =>
        {
            //Progress
            ChangeTurretOutlineWidth(t.CurrentValue);
        });
    }

    #endregion Turret color management

    #region Getters and setters

    public void SetPlanet(GameObject planet)
    {
        m_planet = planet;
    }

    public GameObject GetPlanet()
    {
        return m_planet;
    }

    public Transform GetCameraPositionReference()
    {
        return camPosReference;
    }

    public Color GetCurrentColor()
    {
        return currentColor;
    }

    public Color GetHighlightColor()
    {
        return highlightColor;
    }

    public Color GetSelectedColor()
    {
        return selectedColor;
    }

    public Color GetDefaultColor()
    {
        return defaultColor;
    }

    public Transform[] GetTurretBases()
    {
        return turretBases;
    }

    public Transform[] GetTurretCannons()
    {
        return turretCannons;
    }

    public float GetCurrentOutlineWidth()
    {
        return currentOutlineWidth;
    }

    public float GetDefaultFarOutlineWidth()
    {
        return cameraFarOutlineWidth;
    }

    public float GetDefaultNearOutlineWidth()
    {
        return cameraNearOutlineWidth;
    }

    public bool IsTurretBoosted()
    {
        return turretBoosted;
    }

    public Color GetBoostedTurretHighlight()
    {
        return boostedTurretHightlight;
    }

    public Color GetBoostedTurretColor()
    {
        return boostedTurretColor;
    }

    public Material GetMaterial()
    {
        //We assume that all the meshes of the turret use the same material,
        //so we can simply take the material of the first mesh
        return meshes[0].GetComponent<MeshRenderer>().material;
    }

    public void SetMaterial(Material newMat)
    {
        for (int i = 0; i < meshes.Length; i++)
        {
            meshes[i].GetComponent<MeshRenderer>().material = newMat;
            meshesMaterials[i] = newMat;
        }
    }

    public TURRET_TYPE GetTurretType()
    {
        return currentTurretType;
    }

    #endregion Getters and setters
}