﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretRing : MonoBehaviour
{
    [SerializeField] float basicRadius;
    [SerializeField] float basicHeightOffset;
    [SerializeField] float heavyRadius;
    [SerializeField] float heavyHeightOffset;
    [SerializeField] GameObject planet;

    public float getBasicRadius()
    {
        return basicRadius;
    }

    public float getBasicHeightOffset()
    {
        return basicHeightOffset;
    }

    public float getHeavyRadius()
    {
        return heavyRadius;
    }

    public float getHeayHeightOffset()
    {
        return heavyHeightOffset;
    }

    public GameObject getPlanet()
    {
        return planet;
    }
}
