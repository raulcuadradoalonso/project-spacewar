﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretImpactManager : MonoBehaviour
{
    private CompleteShield shield;

    private void Start()
    {
        shield = GetComponent<CompleteShield>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "EnemyBullet")
        {
            //Take enemy bullet script to get the dmg of the bullet
            float dmg = other.gameObject.GetComponent<Bullet>().GetDamage();
            shield.ReceiveDamage(dmg);

            //Destroy the bullet
            //It is better to destroy it here because we cannot be sure if
            //this function will be executed before the bullet's one (so it could be destroyed before taking its damage)
            //Destroy(other.gameObject);
            other.gameObject.SetActive(false);
        }

        if (other.tag == "Vehicle" && other.GetComponent<Bullet>() != null)
        {
            float dmg = other.gameObject.GetComponent<Bullet>().GetDamage();
            shield.ReceiveDamage(dmg);
            other.GetComponent<Vehicle>().MakeDamage(int.MaxValue);
        }
    }
}