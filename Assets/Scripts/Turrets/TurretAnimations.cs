﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;

public class TurretAnimations : MonoBehaviour
{
    [SerializeField] float animationOffset;
    [SerializeField] AudioClip[] laserShoots;
    [SerializeField] Vector3 directionOffset;

    Transform[] turretCannons;
    Vector3[] animationOffsetValues;
    Vector3[] animationDefaultValues;
    AudioSource[] audioEmitters;

    TurretController tc;

    void Start()
    {
        tc = GetComponent<TurretController>();        

        turretCannons = tc.GetTurretCannons();
        animationOffsetValues = new Vector3[turretCannons.Length];
        animationDefaultValues = new Vector3[turretCannons.Length];
        audioEmitters = new AudioSource[turretCannons.Length];

        setDefaultAnimationOffsets();
    }

    //The amout of movement done by each cannon always is based on an offset from their initial position
    void setDefaultAnimationOffsets()
    {
        for (int i = 0; i < turretCannons.Length; i++)
        {
            animationDefaultValues[i] = turretCannons[i].transform.localPosition;
            animationOffsetValues[i] = turretCannons[i].transform.localPosition - directionOffset * animationOffset;
            audioEmitters[i] = turretCannons[i].GetComponent<AudioSource>();
        }
    }

    public void cannonShootAnimation(int currentIndex, float currentCooldown)
    {
        audioEmitters[currentIndex].PlayOneShot(laserShoots[Random.Range(0, laserShoots.Length)]);

        Vector3 targetPos = animationOffsetValues[currentIndex];
        Vector3 currentPos = turretCannons[currentIndex].transform.localPosition;

        float totalTime = currentCooldown * turretCannons.Length;
        float recoilTime = (1.0f / 4.0f) * totalTime;
        float recoverTime = (3.0f / 4.0f) * totalTime;

        turretCannons[currentIndex].gameObject.Tween("Animation of turret" + turretCannons[currentIndex].gameObject.GetInstanceID(), currentPos, targetPos, recoilTime * 0.75f, TweenScaleFunctions.CubicEaseOut, (t) =>
        {
            //Progress
            if (turretCannons[currentIndex] != null)
            {
                turretCannons[currentIndex].transform.localPosition = t.CurrentValue;
            }
        }, (t) =>
        {
            //Completion
            if (turretCannons[currentIndex] != null)
            {
                targetPos = animationDefaultValues[currentIndex];
                currentPos = turretCannons[currentIndex].transform.localPosition;

                turretCannons[currentIndex].gameObject.Tween("Animation of turret" + turretCannons[currentIndex].gameObject.GetInstanceID(), currentPos, targetPos, recoverTime, TweenScaleFunctions.CubicEaseInOut, (x) =>
                {
                    //Progress
                    if (turretCannons[currentIndex] != null)
                    {
                        turretCannons[currentIndex].transform.localPosition = x.CurrentValue;
                    }
                });
            }
        });
    }
}
