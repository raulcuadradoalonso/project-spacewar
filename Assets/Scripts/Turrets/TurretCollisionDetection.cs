﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretCollisionDetection : MonoBehaviour
{
    //This script is used while positioning turrets to check if the turret collides
    //with another turret. In that case, we cannot set the turret there.

    private bool canBePositioned = true;
    private List<GameObject> turretsColliding = new List<GameObject>();
    private Rigidbody rb;

    private void Start()
    {
        //While we do that checking, we need a rigibody to trigger collision events
        rb = gameObject.AddComponent<Rigidbody>();
        rb.useGravity = false;
        rb.constraints = RigidbodyConstraints.FreezeAll;
        //For that collision events, we also need the collider to be trigger
        GetComponent<BoxCollider>().isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("ManualTurret"))
        {
            turretsColliding.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("ManualTurret"))
        {
            turretsColliding.Remove(other.gameObject);
        }
    }

    //Collider is set to not trigger again to be used as expected
    private void OnDestroy()
    {
        GetComponent<BoxCollider>().isTrigger = false;
        Destroy(rb);
    }

    public bool CanBeSet()
    {
        return turretsColliding.Count == 0;
    }

    public void ResetDetection()
    {
        turretsColliding.Clear();
    }
}